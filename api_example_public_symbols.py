# RA, 2020-05-09

# Example: Retrieve the list of "symbols"

import pandas as pd
from pathlib import Path


# https://api.hitbtc.com/#symbols
# Note: takeLiquidityRate and provideLiquidityRate are *default* rates
import requests
try:
	session = requests.session()
	response = session.get('https://api.hitbtc.com/api/2/public/symbol')
	data = response.json()
	response.raise_for_status()
except requests.exceptions.HTTPError as ex:
	raise

df: pd.DataFrame
df = pd.DataFrame(data=data, dtype=float).set_index('id')
assert (isinstance(df['takeLiquidityRate']['ETHBTC'], float))

# print("Types:")
# print(df.dtypes)

output_filepath = Path(__file__).parent / F"db/{Path(__file__).stem}/list"
output_filepath.parent.mkdir(exist_ok=True, parents=True)

with output_filepath.with_suffix('.txt').open('w') as fd:
	print(df.to_markdown(), file=fd)

with output_filepath.with_suffix('.csv').open('w') as fd:
	df.to_csv(fd)

