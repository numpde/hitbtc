# RA, 2020-06-01

"""
A first model to learn urgency-to-act
from "multiquantized" price.
"""

"""
On cross-validation of time series
https://towardsdatascience.com/time-based-cross-validation-d259b13d42b8
https://scikit-learn.org/stable/modules/cross_validation.html
https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.TimeSeriesSplit.html
"""

from commons import logger, singleton
from commons import Plot
from commons import IdGenerator as IdGenerator

import numpy as np
import pandas as pd

from typing import Tuple
from inclusive import range
from more_itertools import pairwise

import os
import pickle, ast, json
from sqlite3 import connect
from pathlib import Path
from collections import Counter

PARAM = {
    'dataset registry': Path(__file__).parent / "db/trade_history/multiquantize/datasets/datasets.db",
    'datasets dir': Path(__file__).parent / "db/trade_history/multiquantize/datasets/UV/",

    'models dir': Path(__file__).parent / "db/trade_history/multiquantize/models/UV/",

    'symbol': "BTCUSD",
    'train_period': "2020-05",
    'bins up/down': 1,
    # 'random seed': 1,
}


def get_ds_registry() -> pd.DataFrame:
    with connect(PARAM['dataset registry']) as con:
        df = pd.read_sql(con=con, sql="select * from datasets")
        # df = df.set_index('filename')

    def unstr(x):
        try:
            return json.loads(x)
        except json.decoder.JSONDecodeError:
            return ast.literal_eval(x)
        except:
            logger.error(F"Could not parse {x}")
            raise

    # Parse str(dict(...)) fields
    for c in ['historical']:
        df[c] = df[c].apply(unstr)

    return df


def preprocess(X):
    X = np.array([
        np.exp(np.diff(np.log(signal), axis=1)).flatten()
        for signal in X
    ])
    return X


def preprocess_labels(y):
    y = 1 * (np.abs(y) >= 0.001)
    return y


def load_dataset(filepath: Path, dropna=True):
    logger.info(F"Loading dataset from {filepath}")

    with filepath.open('rb') as fd:
        ds_with_meta = pickle.load(fd)

    data = ds_with_meta['data']

    # data['t'] is a list of query times;
    # data['up'] is the "urgency to buy";
    # data['dn'] is the "urgency to sell";
    # data['signal'] is a list of arrays,
    # each array has size nscales x nsamples.

    X = np.asarray([signal for signal in data['signal']])
    y_up = np.reshape(data['up'], (len(X),))
    y_dn = np.reshape(data['dn'], (len(X),))
    t = np.reshape(data['t'], (len(X),)).astype(np.datetime64)
    assert singleton({len(X), len(y_up), len(y_dn), len(t)})

    # Combine ups and downs
    assert all(y_up >= 0)
    assert all(y_dn <= 0)
    y = y_up + y_dn

    logger.debug(F"X.shape = {X.shape}")
    logger.debug(F"y.shape = {y.shape}")

    if dropna:
        ii = [np.all(~np.isnan(signal.flatten())) for signal in X]
        X = X[ii]
        y = y[ii]
    else:
        pass

    logger.debug(F"Got dataset of size {len(X)}")

    return (X, y)


# def bin_labels(y: pd.Series):
#     # Multilabel classification: with same-size bins
#     y: pd.Series
#     labels = list(range[-PARAM['bins up/down'], PARAM['bins up/down']])
#     (y, bins) = pd.qcut(y, q=len(labels), labels=labels, retbins=True)
#
#     if bins is not None:
#         logger.warning(F"Ignoring specified bins {bins}")
#
#     # Bin intervals
#     bins = np.round(bins, decimals=5)
#     bins = pd.Series(index=labels, data=pairwise(bins))
#
#     logger.debug(F"Labels:   \n{Counter(y)}")
#     logger.debug(F"RoI bins: \n{list(bins)}")
#
#     return (y, bins)

def load_dataset_by_period(symbol: str, period: str, most_recent=True) -> Tuple[np.ndarray, np.ndarray, pd.Series]:
    """
    Returns a tuple (X, y, meta)
    """

    if not most_recent:
        raise NotImplementedError

    ds_registry = get_ds_registry()

    # Convert a series of dict's to a dataframe
    # Columns: symbol, period_format, period
    ds_hist = pd.json_normalize(ds_registry.historical).set_index(ds_registry.index)
    ds_hist = ds_hist[ds_hist.symbol == symbol]

    name = max(ds_registry.filename[ds_hist.period == period])
    path = PARAM['datasets dir'] / name

    meta = ds_registry.set_index('filename').loc[name]
    (X, y) = load_dataset(path)

    return (X, y, meta)


# def prepare_datasets():
#     ds_registry = get_ds_registry()
#
#     # Convert a series of dict's to a dataframe
#     # Columns: symbol, period_format, period
#     ds_hist = pd.json_normalize(ds_registry.historical).set_index(ds_registry.index)
#
#     ds_hist = ds_hist[ds_hist.symbol == PARAM['symbol']]
#
#     f0 = PARAM['datasets dir'] / max(ds_registry.filename[ds_hist.period == "2020-05"])
#     f1 = PARAM['datasets dir'] / max(ds_registry.filename[ds_hist.period == "2020-06-04"])
#
#     # logger.debug(F"Training dataset: {f0}")
#     # logger.debug(F"Training dataset: {f1}")
#     ds0_meta = ds_registry[ds_registry.filename == f0.name].squeeze()
#     ds1_meta = ds_registry[ds_registry.filename == f1.name].squeeze()
#     logger.debug(F"Training dataset:   \n{ds0_meta.to_markdown()}")
#     logger.debug(F"Validation dataset: \n{ds1_meta.to_markdown()}")
#
#     (X0, y0) = load_dataset(f0)
#     (X1, y1) = load_dataset(f1)
#
#     return [(X0, y0, ds0_meta), (X1, y1, ds1_meta)]


def cross_validation(X, y):
    # rs = np.random.RandomState(seed=PARAM['random seed'])
    # logger.info(F"Random state: {rs}, seeded with {PARAM['random seed']}")

    from sklearn.utils import compute_sample_weight
    sample_weight = compute_sample_weight('balanced', y)

    logger.info(F"Sample weights: {Counter(sample_weight)}")

    from sklearn.pipeline import Pipeline
    from sklearn.preprocessing import FunctionTransformer
    from sklearn.ensemble import RandomForestRegressor as C
    # from sklearn.ensemble import RandomForestClassifier as C

    # from sklearn.preprocessing import KBinsDiscretizer as KBeans
    # eatembeans = KBeans(n_bins=(2 * PARAM['bins up/down'] + 1), encode='ordinal', strategy='quantile')

    # https://stackoverflow.com/questions/59556305/how-can-i-use-transformedtargetregressor-in-a-gridsearchcv
    from sklearn.compose import TransformedTargetRegressor
    # compo = TransformedTargetRegressor(regressor=clf, transformer=kbeans)

    # y = preprocess_labels(y)

    pipo = Pipeline([
        (
            'preprocess',
            FunctionTransformer(preprocess)
        ),
        (
            'core',
            C(
                criterion="mae",
                n_jobs=8,
                verbose=1,

                # Will be overwritten by CV:
                n_estimators=1, max_depth=1,
            )
        ),
    ])

    # Sanity check
    pipo.fit(X, y)
    logger.debug(F"Sanity check, prediction: [{', '.join(map(str, pipo.predict(X)[0:3]))}, etc.]")

    from sklearn.model_selection import GridSearchCV
    modelparams_grid = [
        # {
        #     # Quick test
        #     'core__n_estimators': [10],
        #     'core__max_depth': [3],
        # },
        # {
        #     # Half-Serious
        #     'core__n_estimators': [50],
        #     'core__max_depth': [10],
        #     'core__random_state': [111111],
        # },
        {
            # Serious
            'core__n_estimators': [5, 10],
            'core__max_depth': [4, 6],
            'core__random_state': [111111],
        },
    ]

    logger.info(F"Cross-validation grid: {modelparams_grid}")

    # https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html

    # # https://scikit-learn.org/stable/modules/generated/sklearn.metrics.matthews_corrcoef.html
    # from sklearn.metrics import matthews_corrcoef, make_scorer
    # from sklearn.metrics import f1_score

    # # https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html
    # scorer = make_scorer(f1_score, sample_weight=sample_weight, average='weighted')

    clf = GridSearchCV(
        pipo,
        param_grid=modelparams_grid,

        # https://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter
        scoring='neg_mean_absolute_error',
        # scoring='neg_root_mean_squared_error',
        # scoring='f1_micro',
        # scoring=make_scorer(matthews_corrcoef),
        # scoring='roc_auc',
        # scoring='f1_weighted',  # seems to be worse than f1
        # scoring=scorer,  # Error: inconsistent numbers of samples (i.e. weights)

        # Cross-validation folds for each combination of hyperparameters
        cv=2,
        # n_jobs=5,

        verbose=0,
    )

    logger.info(F"Starting grid search")

    from commons import stdout_to
    import sys, io
    # with stdout_to(io.BytesIO()):
    clf.fit(X, y)

    logger.info(F"CF results:  \n{pd.DataFrame(clf.cv_results_).to_markdown()}")
    logger.info(F"Best params: \n{clf.best_params_}")

    return clf

    # from sklearn.metrics import confusion_matrix
    # coma = confusion_matrix(y_valid, model.predict(X_valid), sample_weight=None)
    # logger.debug(F"Confusion matrix: \n{coma}")

    # if False:
    #     # For binary classification
    #
    #     from sklearn.metrics import roc_auc_score
    #     roc = roc_auc_score(y_valid, model.predict_proba(X_valid)[:, 1])
    #     print(F"ROC: {roc}")
    #
    #     from sklearn.metrics import roc_curve
    #     (fpr, tpr, thr) = roc_curve(y_valid, model.predict_proba(X_valid)[:, 1])
    #
    #     with Plot() as p:
    #         p.a.plot(fpr, tpr, 'x--')
    #         p.a.set_xlabel("FP rate")
    #         p.a.set_ylabel("TP rate")
    #         p.show()


def main():
    # [
    #     (X_train, y_train, ds_meta_train),
    #     (X_valid, y_valid, ds_meta_valid),
    # ] = prepare_datasets()

    (X_train, y_train, ds_meta_train) = load_dataset_by_period(symbol=PARAM['symbol'], period=PARAM['train_period'])

    # if not (max(X_train.index) < min(X_valid.index)):
    #     logger.warning(F"Training dataset temporally not strictly /before/ validation dataset")

    #
    model = cross_validation(X_train, y_train)

    filepath = (PARAM['models dir'] / F"{next(IdGenerator(prefix='Clf1', zfill=None))}").with_suffix(".pkl")
    filepath.parent.mkdir(exist_ok=True, parents=True)

    model_with_meta = {
        'model': model,

        # Read with pd.read_json(..., typ="series")
        'ds_meta': ds_meta_train.to_json(),

        # 'PARAM': PARAM,
    }

    # raise NotImplementedError

    logger.info(F"Writing model to {os.path.relpath(filepath, Path(__file__).parent)}")
    with filepath.open(mode='wb') as fd:
        pickle.dump(model_with_meta, file=fd)


if __name__ == '__main__':
    main()
