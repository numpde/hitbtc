# RA, 2020-05-09

# Example: Retrieve the order book
# Requires no authentication

# Note: timestamps are ISO 8601
# https://api.hitbtc.com/#datetime-format
#
import pandas as pd
from datetime import datetime as dt
from dateutil.parser import isoparse as fromiso8601

symbols = {"ETHUSD"}

# https://api.hitbtc.com/#order-book
import requests
try:
	session = requests.session()
	url = 'https://api.hitbtc.com/api/2/public/orderbook?symbols={symbols}&limit={limit}'
	url = url.format(symbols=(",".join(symbols)), limit=2)
	response = session.get(url)
	data = response.json()
	response.raise_for_status()
except requests.exceptions.HTTPError as ex:
	raise

# Example:
# data = {
# 	'ETHBTC': {
# 		'symbol': 'ETHBTC',
# 		'ask': [{'price': '0.021985', 'size': '0.0871'}, {'price': '0.021986', 'size': '2.7003'}],
# 		'bid': [{'price': '0.021981', 'size': '27.0551'}, {'price': '0.021980', 'size': '11.0957'}],
# 		'timestamp': '2020-05-09T23:04:07.989Z'
# 	}
# }

order_book = {
	symbol: {
		'ask': pd.DataFrame(orders['ask']).astype(float),
		'bid': pd.DataFrame(orders['bid']).astype(float),
		'dt': fromiso8601(orders['timestamp']),
	}
	for (symbol, orders) in data.items()
}

symbol = symbols.pop()
print(order_book[symbol]['ask'])
print(order_book[symbol]['bid'])
print(order_book[symbol]['dt'])

exit()

df: pd.DataFrame
df = pd.DataFrame(data=data, dtype=float)
print(df.to_markdown())
df = df.set_index('id')
assert (isinstance(df['takeLiquidityRate'][symbol], float))

print("Types:")
print(df.dtypes)

print("Preview:")
print(df.head().to_markdown())

