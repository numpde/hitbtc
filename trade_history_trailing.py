# RA, 2022-09-01

"""
Compare historical ETH/USDT and ETH/USDC rates.
"""

from pathlib import Path
from decimal import Decimal

import numpy as np
import pandas as pd

from plox import Plox

from commons_db import Historical


def quantize_timestamp(df: pd.DataFrame) -> pd.DataFrame:
    return df[['timestamp', 'price', 'quantity']].groupby(
        df['timestamp'].dt.round('1S')
    ).agg(
        price=pd.NamedAgg(column='price', aggfunc=np.median),
        quantity=pd.NamedAgg(column='quantity', aggfunc=np.sum),
    )


PARAM = {
    'output': Path(__file__).parent / "db/trade_history/trailing/",
}

historical = {
    symbol: Historical(symbol=symbol, period_format="%Y-%m", use_api_types=False)
    for symbol in ['ETHUSD', 'ETHUSDC']
}

periods = list(historical['ETHUSD'].keys())

period = '2022-07'

df_usdt = quantize_timestamp(historical['ETHUSD'][period])
df_usdc = quantize_timestamp(historical['ETHUSDC'][period])

# threshold_min_eth = Decimal('1')
# threshold_max_eth = Decimal('10')
#
# df_usdt = df_usdt[(threshold_min_eth <= df_usdt.quantity) & (df_usdt.quantity <= threshold_max_eth)]
# df_usdc = df_usdc[(threshold_min_eth <= df_usdc.quantity) & (df_usdc.quantity <= threshold_max_eth)]

# print(df_usdt)
# print(df_usdc)

# # There is no 'side' after time-quantization
# df_usdt = df_usdt[df_usdt.side == 'sell']
# df_usdc = df_usdc[df_usdc.side == 'sell']

with Plox() as px:
    px.a.plot(df_usdt.index, df_usdt.price, lw=0.5, c='C1', label="ETH/USDT")
    px.a.plot(df_usdc.index, df_usdc.price, lw=0.5, c='C2', label="ETH/USDC")
    px.a.set_xlabel("Time")
    px.a.set_ylabel("Price")
    px.a.legend()
    px.a.set_title(f"Period: {period}")
    px.show()
