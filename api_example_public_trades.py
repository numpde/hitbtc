# RA, 2020-05-10

# GET /api/2/public/trades
# Returns trades information for a symbol
# Requires no API key

from matplotlib import pyplot as plt
from typing import Dict

import matplotlib as mpl
mpl.use("agg")

# Note: timestamps are ISO 8601
# https://api.hitbtc.com/#datetime-format
#
import pandas as pd
from datetime import datetime as dt
from datetime import timedelta
from dateutil.parser import isoparse as fromiso8601

column_types = {
	'id': int,
	'price': float,
	'quantity': float,
	'side': str,
	'timestamp': 'datetime64',  # equivalent to pd.to_datetime
}

# Values for `side`
(SELL, BUY) = ('sell', 'buy')

# Color convention
colors = {SELL: "red", BUY: "green"}

def recent_trades_by_symbol(symbols, ta: dt, limit=100):
	# https://api.hitbtc.com/#trades
	import requests
	try:
		session = requests.session()
		# Parameters:
		# limit <= 1000, default: 100
		# offset <= 100000, default: 0
		url = 'https://api.hitbtc.com/api/2/public/trades?symbols={symbols}&limit={limit}&from={ta}'
		url = url.format(symbols=(",".join(symbols)), limit=limit, ta=ta.isoformat())
		response = session.get(url)
		data = response.json()
		response.raise_for_status()
	except requests.exceptions.HTTPError as ex:
		raise

	trades_by_symbol: Dict[str, pd.DataFrame]
	trades_by_symbol = {
		symbol: pd.DataFrame(data=trades).astype(column_types).set_index('id')
		for (symbol, trades) in data.items()
		if trades
	}

	return trades_by_symbol


symbols = {"BTCUSD"}


# # Show recent buy/sell prices
#
# (fig, ax1) = plt.subplots()
# plt.ion()
# plt.show()
#
# ta = (dt.utcnow() - timedelta(minutes=1))
#
# for __ in range(20):
# 	trades_by_symbol = recent_trades_by_symbol(symbols, ta=ta)
#
# 	for (symbol, trades) in trades_by_symbol.items():
# 		sell = trades[trades['side'] == SELL]
# 		buy = trades[trades['side'] == BUY]
# 		ax1.plot(sell.timestamp, sell.price, 'r.--')
# 		ax1.plot(buy.timestamp, buy.price, 'g.--')
#
# 		ta = max(ta, max(trades['timestamp']))
#
#
# 	plt.pause(5)

# Show a histogram of buy / sell
ta = dt.utcnow() - timedelta(minutes=100)
trades_by_symbol = recent_trades_by_symbol(symbols, ta=ta, limit=1000)
for (symbol, trades) in trades_by_symbol.items():
	(fig, ax1) = plt.subplots()

	for side in (SELL, BUY):
		ax1.hist(trades[trades['side'] == side].quantity, fill=False, ec=colors[side], label=side)

	ax1.set_yscale('log')
	ax1.set_xlabel(F"Quantity of {symbol}")
	ax1.set_ylabel("Number of orders")
	ax1.legend()

plt.show()
