# RA, 2020-05-20

from commons import stdout_to

import numpy as np
import pandas as pd
import inspect

from typing import Iterable
from pathlib import Path
from sqlite3 import connect
from datetime import datetime, timedelta

# https://sqlbuilder.readthedocs.io/en/latest/
from sqlbuilder.smartsql import T, Q, F, func as Func

# https://stackoverflow.com/a/55836584/3609568
from statsmodels.tsa.stattools import acf as autocorrelation

import matplotlib as mpl

mpl.use("Agg")

import matplotlib.pyplot as plt

PARAM = {
    'db': Path(__file__).parent / "db/trade_history/UV/symbols.db",

    'output': Path(__file__).parent / "db/trade_history/autocorrelation/",
    'savefig': dict(bbox_inches='tight', pad_inches=0, transparent=False),
}


def list_sqlite_tables(con) -> pd.DataFrame:
    return pd.read_sql_query("SELECT * FROM sqlite_master WHERE type='table'", con)


def only(df: pd.DataFrame, side: str):
    assert side in ["buy", "sell"]
    return df[df.side == side].sort_values(by='timestamp').drop_duplicates()


def compile_query(q: Q):
    from sqlbuilder.smartsql.dialects.sqlite import compile as compile_sql_
    return dict(zip(['sql', 'params'], compile_sql_(q)))


def minmax_timestamp(con, symbol):
    """
    Returns a DataFrame with min and max "timestamp"s in table symbol
    """
    sql = compile_query(
        Q(T(symbol)).fields(
            Func.MIN(F('timestamp')).as_('min'),
            Func.MAX(F('timestamp')).as_('max'),
        )
    )
    return pd.to_datetime(pd.read_sql_query(con=con, **sql).iloc[0]).dt.to_pydatetime()


def foobar1(con, symbol):
    """
    Auto-correlate at different sampling frequencies
    """

    # Process the database month by month
    periods: Iterable[pd.Period]
    periods = sorted(set(pd.date_range(*minmax_timestamp(con, symbol), freq='D').to_period('M')))
    period2dt = (lambda p: p.to_timestamp().to_pydatetime())

    for period in periods:
        print(F"Processing {symbol} for period: {period}")

        query = compile_query(
            Q(T(symbol)).fields("*").where(
                (period2dt(period) <= F('timestamp')) & (F('timestamp') < period2dt(period + 1))
            )
        )

        # SQL CALL
        df = pd.read_sql_query(con=con, parse_dates=['timestamp'], **query)

        assert type(df.timestamp) is not str

        # Actual timeseries window
        ta = min(df.timestamp)
        tb = max(df.timestamp)

        # Directory for output
        output_basepath = PARAM['output'] / F"{symbol}/"
        output_basepath.mkdir(parents=True, exist_ok=True)

        for side in ['buy', 'sell']:
            output_filepath = output_basepath / F"{period}_{side}"

            df_side = only(df, side=side)
            price_on = (lambda x: np.interp(x, xp=df_side['timestamp'], fp=df_side['price']))

            fig: plt.Figure
            ax1: plt.Axes
            (fig, ax1) = plt.subplots()

            seconds = (lambda s: timedelta(seconds=s))
            for wdw in map(seconds, [0.25, 0.5, 1, 2, 4, 8, 16, 32]):
                yy = [
                    # Autocorrelate returns for different lags
                    autocorrelation(
                        # Sample returns across time intervals `wdw`
                        np.exp(np.diff(np.log(price_on(pd.date_range(ta + offset, tb, freq=wdw))))),
                        nlags=(seconds(2 ** 8) / wdw),
                        fft=True, missing='raise',
                    )
                    for offset in pd.timedelta_range(0, wdw, periods=5)[:-1]
                ]

                yy_avg = np.mean(yy, axis=0)
                yy_err = 1.96 * np.std(yy, axis=0)

                # Plot autocorrelation
                tt = (wdw / seconds(1)) * np.arange(len(yy_avg))
                ax1.errorbar(
                    tt[1:], yy_avg[1:],
                    yerr=yy_err[1:], marker='.', linestyle='--',
                    label=F"Sample freq: {wdw.total_seconds()}s"
                )

            ax1.set_ylim(*[-0.1, 0.5])
            ax1.set_xscale('log')
            ax1.set_xlabel('Lag (seconds)')
            ax1.grid()
            ax1.legend()

            fig.savefig(output_filepath.with_suffix(".png"), **PARAM['savefig'])
            plt.close(fig)

            with output_filepath.with_suffix(".txt").open(mode='w') as fd:
                with stdout_to(fd):
                    print("SQL: ")
                    print(query)
                    print("=" * 80)
                    print("Dataframe summary:")
                    print(df_side.describe(include='all').to_markdown())
                    print("=" * 80)
                    print("Code:")
                    print(inspect.getsource(inspect.currentframe()))


def main():
    with connect(PARAM['db']) as con:
        for symbol in list_sqlite_tables(con).name:
            foobar1(con, symbol)


if __name__ == '__main__':
    main()
