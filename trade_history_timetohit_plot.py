# RA, 2020-05-28

from pathlib import Path
from decimal import Decimal

from commons import logger, Plot

import numpy as np
import pandas as pd
import seaborn as sns

from datetime import timedelta

basepaths = (Path(__file__).parent / "db/trade_history/timetohit/").glob("*")

for basepath in basepaths:
    logger.info(F"{basepath}")
    sources = sorted((basepath / "UV").glob("*.csv"))
    target_folder = basepath / "figs"
    target_folder.mkdir(exist_ok=True, parents=True)

    for src in sources:
        logger.debug(F"Processing {src.name}")
        trades: pd.DataFrame
        # trades = next(iter(pd.read_csv(src, sep='\t', usecols=['timestamp', '+', '-'], parse_dates=['timestamp'], chunksize=100000)))
        trades = pd.read_csv(src, sep='\t', usecols=['timestamp', '+', '-'], parse_dates=['timestamp'])
        trades = trades.set_index('timestamp').sort_index()

        for (period, df) in trades.groupby(lambda i: i.strftime("%Y-%m")):
            logger.info(F"period = {period}")

            df = pd.DataFrame(data={
                c: pd.Series(
                    df[c] * timedelta(seconds=1) / timedelta(days=1)
                ).apply(
                    lambda x: "NA" if np.isnan(x) else "{:02}".format(Decimal(np.ceil(x)))
                ).value_counts(
                    normalize=True, dropna=False
                )
                for c in df.columns
            })

            df = df.reset_index().melt(id_vars="index", var_name='+/-', value_name='freq')
            df = df.fillna(0)

            with Plot() as p:
                # Unpivot for seaborn

                sns.barplot(
                    x='index', y='freq', hue='+/-',
                    data=df,
                    ax=p.a,
                    hue_order=['+', '-'], palette=['g', 'r'],
                )

                # sns.despine(p.f)

                # (__, jump) = re.fullmatch(r"(.*)_(.*)", str(src.stem)).groups()
                # fac = F"{1 + Decimal(jump[1:])}x " + {'-': "down", "+": "up"}[jump[0]]
                # p.a.set_xlabel(F"Time-to-hit {fac} (days)")

                p.f.savefig(target_folder / F"{str(src.stem)}_{period}.png", )
