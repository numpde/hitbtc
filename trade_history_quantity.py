# RA, 2020-05-24

"""
Histograms of trade volumes
"""

import pandas as pd
import numpy as np
from pathlib import Path
from sqlite3 import connect

from commons import logger, singleton

from sqlbuilder.smartsql import Q, F, Table, func as Func

import matplotlib.pyplot as plt

logger.basicConfig(level='DEBUG', format="[%(levelname).1s] %(message)s")
logger.getLogger("matplotlib").setLevel("WARNING")

PARAM = {
    'db': Path(__file__).parent / "db/trade_history/UV/symbols.db",

    'out_quantity_hist': Path(__file__).parent / "db/trade_history/quantity/",
    'savefig': dict(bbox_inches='tight', pad_inches=0, transparent=False),
}



def list_sqlite_tables(con) -> pd.DataFrame:
    tables = pd.read_sql_query(con=con, sql="select * from sqlite_master where type='table'")
    assert len(tables)
    return tables


def execute_query(con, query, parse_dates="timestamp") -> pd.DataFrame:
    from sqlbuilder.smartsql.dialects.sqlite import compile
    (sql, params) = compile(query)
    logger.debug(F"Executing: {query}")
    df = pd.read_sql_query(con=con, sql=sql, params=params, parse_dates=parse_dates)
    logger.debug(F"Done; returning {len(df)} records")
    return df


def plot1(df: pd.DataFrame, symbol):
    period = singleton(df.period)
    logger.info(F"Making a histogram for period {period} from {len(df)} records")

    fig: plt.Figure
    ax1: plt.Axes
    (fig, ax1) = plt.subplots()

    (__, bins) = np.histogram(
        a=df.quantity,
        bins=np.logspace(np.log10(min(df.quantity)), np.log10(max(df.quantity)), 33)
    )

    counts = {
        side: df[df.side == side].quantity
        for (i, side) in enumerate(set(df.side))
    }
    ax1.hist(x=list(counts.values()), bins=bins, ec="k", label=list(counts.keys()))
    ax1.legend()

    ax1.set_xscale('log')
    ax1.set_yscale('log')

    ax1.set_xlabel(F"Quantity ({symbol}, {period})")
    ax1.set_ylabel(F"Counts")

    filepath = PARAM['out_quantity_hist'] / F"{symbol}/{period}.ext"
    filepath.parent.mkdir(exist_ok=True, parents=True)
    fig.savefig(filepath.with_suffix(".png"), **PARAM['savefig'])
    plt.close(fig)


def foobar1(con, symbol):
    get = (lambda query: execute_query(con, query))

    columns = list(get(
        Q(Table(symbol)).fields('*').limit(0)
    ).columns)
    assert (columns == ['id', 'price', 'quantity', 'side', 'timestamp'])

    # Get a list periods
    period_spec = Func.strftime("%Y=%W", F("timestamp"))

    # # Raw SQL way:
    # periods = pd.read_sql_query(con=con, sql=F"""
    #     select distinct strftime('{period_spec.args[0]}', "timestamp") as "period" from {symbol} order by 1
    #     """
    # )
    # Using SQL builder:
    periods = get(
        Q(Table(symbol)).fields(period_spec.distinct().as_("period")).order_by(F(1).asc())
    ).period
    logger.info(F"Number of periods: {len(periods)}")

    # Section table by period
    for period in periods:
        df = get(
            Q(Table(symbol))
                .fields("*", period_spec.as_("period"))
                .where(period_spec == period)
                .order_by(F(1).asc())
        )

        plot1(df, symbol)


def main():
    with connect(PARAM['db']) as con:
        for symbol in reversed(list_sqlite_tables(con).name):
            logger.info(F"Processing {symbol}")
            foobar1(con, symbol)


if __name__ == '__main__':
    main()
