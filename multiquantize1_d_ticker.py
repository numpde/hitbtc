# RA, 2020-06-04

"""
Use the multiquantize1 predictor
on real-time price
"""

from commons import logger
import trade_history_download
import multiquantize1_a_make_dataset
import multiquantize1_b_train

from inclusive import range

import requests
import pickle

import numpy as np
import pandas as pd

import os
from pathlib import Path
from datetime import datetime, timedelta, timezone

ROOT_PATH = Path(__file__).parent


def get_live_price(symbol, session):
    tw = timedelta(seconds=(16 * (2 ** 5)))
    tb = trade_history_download.now()
    ta = tb - tw
    trades = trade_history_download.get_trades(symbol=symbol, ta=ta, tb=tb, session=session)
    price = multiquantize1_a_make_dataset.price_by_timestamp(trades[['timestamp', 'price']])
    return price


# Required by the model
from multiquantize1_b_train import preprocess


def main():
    symbol = "BTCUSD"
    with requests.session() as session:
        price = get_live_price(symbol, session)
        # logger.debug(F"Recent price: \n{price}")

    signal_extractor = multiquantize1_a_make_dataset.SignalExtractor(
        nscales=6, nsamples=16, max_res=timedelta(seconds=1)
    )

    filepath = max((ROOT_PATH / "db/trade_history/multiquantize/models/UV/").glob("*.pkl"))
    logger.info(F"Loading model from: {os.path.relpath(filepath, ROOT_PATH)}")

    with filepath.open(mode='rb') as fd:
        model_with_meta = pickle.load(fd)

    model = model_with_meta['model']

    logger.info(F"Model: {model}")

    for n in range[-20, 0]:
        t = max(price.index) - timedelta(seconds=n)
        signal = pd.DataFrame(signal_extractor(price, t))
        nna = signal.isna().sum().sum()
        # logger.info(F"Signal at {t}: \n{signal}")
        signal = signal.ffill(axis=1).bfill(axis=0).bfill(axis=1)
        # signal = multiquantize1_b_train.preprocess(signal).flatten()
        y = model.predict([signal])
        logger.debug(F"At n = {n} seconds, model prediction is: {y} (signal had {nna} nan's)")


if __name__ == '__main__':
    main()
