SQL: 
{'sql': 'SELECT * FROM `ETHBTC` WHERE `timestamp` >= ? AND `timestamp` < ?', 'params': [datetime.datetime(2020, 2, 1, 0, 0), datetime.datetime(2020, 3, 1, 0, 0)]}
================================================================================
Dataframe summary:
|        |               id |           price |     quantity | side   | timestamp                  |
|:-------|-----------------:|----------------:|-------------:|:-------|:---------------------------|
| count  | 237830           | 237830          | 237830       | 237830 | 237830                     |
| unique |    nan           |    nan          |    nan       | 1      | 183133                     |
| top    |    nan           |    nan          |    nan       | sell   | 2020-02-03 05:02:08.535000 |
| freq   |    nan           |    nan          |    nan       | 237830 | 51                         |
| first  |    nan           |    nan          |    nan       | nan    | 2020-02-01 00:00:01.762000 |
| last   |    nan           |    nan          |    nan       | nan    | 2020-02-29 23:59:57.342000 |
| mean   |      7.76074e+08 |      0.0251678  |      5.34928 | nan    | nan                        |
| std    |      6.10524e+06 |      0.00232154 |     43.1859  | nan    | nan                        |
| min    |      7.638e+08   |      0.019168   |      0.0001  | nan    | nan                        |
| 25%    |      7.71656e+08 |      0.022949   |      0.0193  | nan    | nan                        |
| 50%    |      7.76108e+08 |      0.025947   |      0.1785  | nan    | nan                        |
| 75%    |      7.81363e+08 |      0.027      |      1.13638 | nan    | nan                        |
| max    |      7.86642e+08 |      0.028482   |    905.006   | nan    | nan                        |
================================================================================
Code:
def minmax_timestamp(symbol):
    sql = compile_query(
        Q(T(symbol)).fields(
            Func.MIN(F('timestamp')).as_('min'),
            Func.MAX(F('timestamp')).as_('max'),
        )
    )

    with connect(PARAM['db']) as con:
        return pd.to_datetime(pd.read_sql_query(con=con, **sql).iloc[0]).dt.to_pydatetime()

