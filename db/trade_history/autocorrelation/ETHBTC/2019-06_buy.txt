SQL: 
{'sql': 'SELECT * FROM `ETHBTC` WHERE `timestamp` >= ? AND `timestamp` < ?', 'params': [datetime.datetime(2019, 6, 1, 0, 0), datetime.datetime(2019, 7, 1, 0, 0)]}
================================================================================
Dataframe summary:
|        |               id |           price |    quantity | side   | timestamp                  |
|:-------|-----------------:|----------------:|------------:|:-------|:---------------------------|
| count  | 479255           | 479255          | 479255      | 479255 | 479255                     |
| unique |    nan           |    nan          |    nan      | 1      | 335628                     |
| top    |    nan           |    nan          |    nan      | buy    | 2019-06-08 00:59:49.515000 |
| freq   |    nan           |    nan          |    nan      | 479255 | 98                         |
| first  |    nan           |    nan          |    nan      | nan    | 2019-06-01 00:00:09.906000 |
| last   |    nan           |    nan          |    nan      | nan    | 2019-06-30 23:59:58.789000 |
| mean   |      5.87807e+08 |      0.0286403  |     11.9    | nan    | nan                        |
| std    |      1.06488e+07 |      0.00214676 |     64.7512 | nan    | nan                        |
| min    |      5.68657e+08 |      0.024983   |      0.0001 | nan    | nan                        |
| 25%    |      5.77846e+08 |      0.026464   |      0.0131 | nan    | nan                        |
| 50%    |      5.90082e+08 |      0.028845   |      0.5    | nan    | nan                        |
| 75%    |      5.97094e+08 |      0.030838   |      3      | nan    | nan                        |
| max    |      6.028e+08   |      0.032276   |    901.693  | nan    | nan                        |
================================================================================
Code:
def foobar1(symbol):
    """
    Auto-correlate at different sampling frequencies
    """

    # Process the database month by month
    periods: Iterable[pd.Period]
    periods = sorted(set(pd.date_range(*minmax_timestamp(symbol), freq='D').to_period('M')))
    period2dt = (lambda p: p.to_timestamp().to_pydatetime())

    for period in periods:
        print(F"Processing {symbol} for period: {period}")

        query = compile_query(
            Q(T(symbol)).fields("*").where(
                (period2dt(period) <= F('timestamp')) & (F('timestamp') < period2dt(period + 1))
            )
        )

        with connect(PARAM['db']) as con:
            df = pd.read_sql_query(con=con, parse_dates=['timestamp'], **query)

        assert type(df.timestamp) is not str

        # Actual timeseries window
        ta = min(df.timestamp)
        tb = max(df.timestamp)

        # Directory for output
        output_basepath = PARAM['output'] / F"{symbol}/"
        output_basepath.mkdir(parents=True, exist_ok=True)

        for side in ['buy', 'sell']:
            output_filepath = output_basepath / F"{period}_{side}"

            df_side = only(df, side=side)
            price_on = (lambda x: np.interp(x, xp=df_side['timestamp'], fp=df_side['price']))

            fig: plt.Figure
            ax1: plt.Axes
            (fig, ax1) = plt.subplots()

            seconds = (lambda s: timedelta(seconds=s))
            for wdw in map(seconds, [0.25, 0.5, 1, 2, 4, 8, 16, 32]):
                yy = [
                    # Autocorrelate returns for different lags
                    autocorrelation(
                        # Sample returns across time intervals `wdw`
                        np.exp(np.diff(np.log(price_on(pd.date_range(ta + offset, tb, freq=wdw))))),
                        nlags=(seconds(2 ** 8) / wdw),
                        fft=True, missing='raise',
                    )
                    for offset in pd.timedelta_range(0, wdw, periods=5)[:-1]
                ]

                yy_avg = np.mean(yy, axis=0)
                yy_err = 1.96 * np.std(yy, axis=0)

                # Plot autocorrelation
                tt = (wdw / seconds(1)) * np.arange(len(yy_avg))
                ax1.errorbar(tt[1:], yy_avg[1:], yerr=yy_err[1:], marker='.', linestyle='--',
                             label=F"Sample freq: {wdw.total_seconds()}s")

            ax1.set_ylim([-0.1, 0.5])
            ax1.set_xscale('log')
            ax1.set_xlabel('Lag (seconds)')
            ax1.grid()
            ax1.legend()

            fig.savefig(output_filepath.with_suffix(".png"), **PARAM['savefig'])
            plt.close(fig)

            with output_filepath.with_suffix(".txt").open(mode='w') as fd:
                with redirect_stdout(fd):
                    print("SQL: ")
                    print(query)
                    print("=" * 80)
                    print("Dataframe summary:")
                    print(df_side.describe(include='all').to_markdown())
                    print("=" * 80)
                    print("Code:")
                    print(inspect.getsource(inspect.currentframe()))

