SQL: 
{'sql': 'SELECT * FROM `BTCUSD` WHERE `timestamp` >= ? AND `timestamp` < ?', 'params': [datetime.datetime(2019, 12, 1, 0, 0), datetime.datetime(2020, 1, 1, 0, 0)]}
================================================================================
Dataframe summary:
|        |               id |      price |      quantity | side   | timestamp                  |
|:-------|-----------------:|-----------:|--------------:|:-------|:---------------------------|
| count  | 366987           | 366987     | 366987        | 366987 | 366987                     |
| unique |    nan           |    nan     |    nan        | 1      | 275480                     |
| top    |    nan           |    nan     |    nan        | buy    | 2019-12-13 16:43:32.902000 |
| freq   |    nan           |    nan     |    nan        | 366987 | 45                         |
| first  |    nan           |    nan     |    nan        | nan    | 2019-12-01 00:00:08.478000 |
| last   |    nan           |    nan     |    nan        | nan    | 2019-12-31 23:59:02.994000 |
| mean   |      7.2865e+08  |   7272.32  |      1.39576  | nan    | nan                        |
| std    |      6.66827e+06 |    195.878 |      8.25058  | nan    | nan                        |
| min    |      7.19349e+08 |   6436     |      1e-05    | nan    | nan                        |
| 25%    |      7.22731e+08 |   7194.1   |      0.00515  | nan    | nan                        |
| 50%    |      7.26867e+08 |   7298.36  |      0.04707  | nan    | nan                        |
| 75%    |      7.3454e+08  |   7389.28  |      0.207825 | nan    | nan                        |
| max    |      7.41967e+08 |   7751.53  |    130.104    | nan    | nan                        |
================================================================================
Code:
def foobar1(symbol):
    """
    Auto-correlate at different sampling frequencies
    """

    # Process the database month by month
    periods: Iterable[pd.Period]
    periods = sorted(set(pd.date_range(*minmax_timestamp(symbol), freq='D').to_period('M')))

    for period in periods:
        print(F"Processing period: {period}")

        period2dt = (lambda p: p.to_timestamp().to_pydatetime())

        sql = compile_query(
            Q(T(symbol)).fields("*").where(
                (period2dt(period) <= F('timestamp')) & (F('timestamp') < period2dt(period + 1))
            )
        )

        with connect(PARAM['db']) as con:
            df = pd.read_sql_query(con=con, parse_dates=['timestamp'], **sql)

        assert type(df.timestamp) is not str

        # Actual timeseries window
        ta = min(df.timestamp)
        tb = max(df.timestamp)

        # Directory for output
        output_basepath = Path(__file__).parent / F"db/trade_history/autocorrelation/{symbol}/"
        output_basepath.mkdir(parents=True, exist_ok=True)

        for side in ['buy', 'sell']:
            output_filepath = output_basepath / F"{period}_{side}"

            df_side = only(df, side=side)
            price_on = (lambda x: np.interp(x, xp=df_side['timestamp'], fp=df_side['price']))

            fig: plt.Figure
            ax1: plt.Axes
            (fig, ax1) = plt.subplots()

            seconds = (lambda s: timedelta(seconds=s))
            for wdw in map(seconds, [0.25, 0.5, 1, 2, 4, 8, 16, 32]):
                yy = [
                    # Autocorrelate returns for different lags
                    autocorrelation(
                        # Sample returns across time intervals `wdw`
                        np.exp(np.diff(np.log(price_on(pd.date_range(ta + offset, tb, freq=wdw))))),
                        nlags=(seconds(2 ** 8) / wdw),
                        fft=True, missing='raise',
                    )
                    for offset in pd.timedelta_range(0, wdw, periods=5)[:-1]
                ]

                yy_avg = np.mean(yy, axis=0)
                yy_err = 1.96 * np.std(yy, axis=0)

                # Plot autocorrelation
                tt = (wdw / seconds(1)) * np.arange(len(yy_avg))
                ax1.errorbar(tt[1:], yy_avg[1:], yerr=yy_err[1:], marker='.', linestyle='--',
                             label=F"Sample freq: {wdw.total_seconds()}s")

            ax1.set_ylim([-0.1, 0.5])
            ax1.set_xscale('log')
            ax1.set_xlabel('Lag (seconds)')
            ax1.grid()
            ax1.legend()

            fig.savefig(output_filepath.with_suffix(".png"), **PARAM['savefig'])
            plt.close(fig)

            with output_filepath.with_suffix(".txt").open(mode='w') as fd:
                with redirect_stdout(fd):
                    print("SQL: ")
                    print(sql)
                    print("=" * 80)
                    print("Dataframe summary:")
                    print(df_side.describe(include='all').to_markdown())
                    print("=" * 80)
                    print("Code:")
                    print(inspect.getsource(inspect.currentframe()))

