import numpy as np
import pandas as pd

from commons import logger as log
from commons_db import Historical

symbol = "ETHUSD"

historical = Historical(symbol=symbol, period_format="%Y-%m", use_api_types=False)

df = historical[max(historical.keys())]

df = df.groupby(
    # Quantize time
    df.timestamp.dt.round('1S')
).agg(
    # Approximate trading price as the window median
    price=pd.NamedAgg(column='price', aggfunc=np.median),
).price

log.info(f"\n{df}")

