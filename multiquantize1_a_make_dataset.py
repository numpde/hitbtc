# RA, 2020-06-01

"""
Make trade-history snippets quantized at different temporal resolutions
to provide a rectangular signal array.
Annotate with urgency-to-act.

Note: joblib.Parallel requires LOTS of disk space at temporary directory
when PARALLEL_N_JOBS > 1.
"""

PARALLEL_N_JOBS = 1

from algorithms import multiquantize, windowed_forward_max
from commons import logger, this_module_body
from commons import Plot
from commons import IdGenerator as IdGenerator
from commons_db import Historical

import numpy as np
import pandas as pd

from progressbar import progressbar
from joblib import Parallel, delayed

import os
import json
import pickle
from sqlite3 import connect
from tempfile import gettempdir

from contextlib import contextmanager
from datetime import datetime, timedelta, timezone
from pathlib import Path

ROOT_PATH = Path(__file__).parent

PARAM = {
    'output_dir': ROOT_PATH / "db/trade_history/multiquantize/datasets/",
    'dataset_records': "datasets.db",
}

DATASET_SPEC = {
    'timestamp': datetime.now(tz=timezone.utc).isoformat(sep=' '),
    'filepath': os.path.relpath(PARAM['output_dir'] / "UV", ROOT_PATH),
    'filename': F"{next(IdGenerator(prefix='MQ00'))}.pkl",
    'historical': {
        'symbol': "BTCUSD",
        'period_format': "%Y-%m-%d",
        'period': "2020-06-04",
    },
    'random_seed': 12,
    'parallel_n_jobs': PARALLEL_N_JOBS,
    'size': 5555,
    'datafields': {
        't': "Query time",
        'up': "Max RoI at query time, within the urgency window (e.g. +0.003)",
        'dn': "Min RoI at query time, within the urgency window (e.g. -0.002)",
        'signal': "Preceding price: nscales x nsample array of nsamples samples at nscales temporal resolutions",
    },
    'meta_sample': {
        'nscales': 6,
        'nsamples': 16,
        'max res (s)': 1,
    },
    'meta_label': {
        'urgency window (s)': timedelta(minutes=1).total_seconds(),
    },
}


def price_by_timestamp(df: pd.DataFrame):
    price = df.price.groupby(df.timestamp.dt.round('1S')).median()
    return price


# def show_mq_signal(signal: np.ndarray) -> Plot:
#     with Plot() as p:
#         p.a.imshow(signal)
#         p.a.set_xticklabels()
#         yield p

def compute_urgency(price: pd.Series, window: timedelta):
    # RoI up and down
    urgency = pd.DataFrame(
        data={
            'up': +windowed_forward_max(+price, window=window) / price - 1,
            'dn': -windowed_forward_max(-price, window=window) / price - 1,
        }
    )
    return urgency


class SignalExtractor:
    def __init__(self, nscales: int, nsamples: int, max_res: timedelta):
        self.nscales = nscales
        self.nsamples = nsamples
        self.max_res = max_res

    def __call__(self, price: pd.Series, t: datetime) -> np.ndarray:
        (signal, price_t) = multiquantize(
            price, tb=t,
            nscales=self.nscales,
            nsamples=self.nsamples,
            max_resolution=self.max_res,
        )
        return signal


def make_datasets():
    dataset_file = ROOT_PATH / DATASET_SPEC['filepath'] / DATASET_SPEC['filename']
    dataset_file.parent.mkdir(exist_ok=True, parents=True)

    registry_file = PARAM['output_dir'] / PARAM['dataset_records']
    registry_file.parent.mkdir(exist_ok=True, parents=True)

    with connect(registry_file) as con:
        dataset_registry = pd.read_sql(con=con, sql="select * from datasets", index_col='filename')
        logger.debug("\n".join(["Dataset registry:", dataset_registry.to_markdown()]))

    dataset_registry = pd.read_json(json.dumps([DATASET_SPEC])).set_index('filename')
    logger.info(F"New dataset spec: \n{dataset_registry.to_markdown()}")

    # Convert some columns to str type
    # To undo, use ast.literal_eval
    cc = dataset_registry.columns[dataset_registry.dtypes == object]
    dataset_registry[cc] = dataset_registry[cc].astype(str)
    # Example:
    assert type(dataset_registry['datafields'].array[0]) is str

    h = Historical(
        symbol=DATASET_SPEC['historical']['symbol'],
        period_format=DATASET_SPEC['historical']['period_format'],
        use_api_types=False,
    )

    logger.info(F"Loading historical data")
    price = price_by_timestamp(h[DATASET_SPEC['historical']['period']])

    urgency_window = timedelta(seconds=DATASET_SPEC['meta_label']['urgency window (s)'])
    urgency = compute_urgency(price, window=urgency_window)

    signal_nscales = DATASET_SPEC['meta_sample']['nscales']
    signal_nsamples = DATASET_SPEC['meta_sample']['nsamples']
    signal_max_res = timedelta(seconds=DATASET_SPEC['meta_sample']['max res (s)'])
    tmin = min(urgency.index) + timedelta(seconds=(signal_nsamples * (2 ** (signal_nscales - 1))))
    tmax = max(urgency.index) - timedelta(minutes=1)
    assert (tmin < tmax)

    urgency = urgency.loc[(tmin < urgency.index) & (urgency.index < tmax)]
    assert len(urgency)

    # Note: output may not be deterministic due to Parallel
    rs = np.random.RandomState(seed=DATASET_SPEC['random_seed'])

    # This array determines the pairs (t, u)
    # where t is the query time
    # and u is urgency-to-buy = one-minute RoI
    # TODO: cleverer-than-random subsets?
    urgency = urgency.sample(n=DATASET_SPEC['size'], random_state=rs, replace=False).sort_index()
    assert len(urgency)

    dataset = {
        'code': this_module_body(),
        'nsamples': len(urgency),
        'signal_shape': (signal_nscales, signal_nsamples),
        'timestamp': DATASET_SPEC['timestamp'],
        'data': {
            't': [], 'up': [], 'dn': [], 'signal': [],
        },
    }

    assert (set(dataset['data'].keys()) == set(DATASET_SPEC['datafields'].keys()))

    signal_extractor = SignalExtractor(signal_nscales, signal_nsamples, signal_max_res)

    logger.info(F"Generating dataset")

    signals = Parallel(
        n_jobs=PARALLEL_N_JOBS,
        temp_folder=gettempdir(),
    )(
        delayed(signal_extractor)(price, t)
        for t in progressbar(list(urgency.index))
    )

    data = dataset['data']
    data['t'] = list(urgency.index)
    data['up'] = list(urgency.up)
    data['dn'] = list(urgency.dn)
    assert type(signals) is list
    data['signal'] = signals

    # price_cheat = price[(t <= price.index) & (price.index <= t + urgency_window)]
    # with Plot() as p_price, show_mq_signal(signal) as p:
    #     p_price.a.plot(price_t)
    #     p_price.a.plot(price_cheat)
    #
    #     p.a.set_title(F"Urgency: {u}")
    #     p.show()

    logger.debug(F"Saving dataset")
    with open(dataset_file, mode='wb') as fd:
        pickle.dump(dataset, fd)

    logger.debug(F"Saving dataset metainfo to registry")
    with connect(registry_file) as con:
        dataset_registry.to_sql(con=con, name="datasets", if_exists="append", )

    logger.info(F"Finished happily")


def main():
    make_datasets()


if __name__ == '__main__':
    main()
