# RA, 2020-05-25

import os
import sys
import time
import typing
import numpy as np
import pandas as pd
import contextlib
import matplotlib.pyplot as plt

from pathlib import Path
from datetime import datetime, timedelta, timezone

# Logger may be overwritten below
import logging as logger

# logger.basicConfig(level=logger.DEBUG, format="[%(levelname).1s] %(message)s")
logger.getLogger("matplotlib").setLevel("WARNING")

#
os.nice(10)


# https://stackoverflow.com/questions/34491808/how-to-get-the-current-scripts-code-in-python
# https://docs.python.org/3/library/inspect.html
def this_module_body(goback=1):
    import inspect
    return inspect.getsource(inspect.getmodule(inspect.stack()[goback].frame))


def stdflush():
    sys.stderr.flush()
    sys.stdout.flush()


@contextlib.contextmanager
def stdout_to(stream):
    sys.stdout = stream
    yield
    sys.stdout = sys.__stdout__


def _initialize_logger(log_dir: Path):
    import logging.config

    try:
        main = Path(sys.modules['__main__'].__file__).stem
    except:
        raise RuntimeError("What is `main`, I don't know")

    log_file = (log_dir / datetime.utcnow().strftime(F"{main}/%Y%m%d/%H%M%S_%f")).with_suffix(".log")
    log_file.parent.mkdir(exist_ok=True, parents=True)

    for mod in ['PIL', 'matplotlib']:
        logging.getLogger(mod).setLevel(logging.WARNING)

    logging.config.dictConfig(dict(
        version=1,
        formatters={
            'style1': {
                'format': "[%(levelname).1s] %(message)s",
                'datefmt': "%H:%M:%S %Z",
            },
            'style2': {
                'format': "[%(levelname).1s] [%(asctime)s] %(message)s",
                'datefmt': "%Y-%m-%d %H:%M:%S %Z",
            },
        },
        handlers={
            's': {
                'class': "logging.StreamHandler",
                'formatter': "style1",
                'level': logging.DEBUG,
                # Note: progressbar uses stderr?
                'stream': "ext://sys.stderr",
            },
            'f': {
                'class': "logging.FileHandler",
                'formatter': "style2",
                'level': logging.DEBUG,
                'filename': log_file,
            },
        },
        root={
            'handlers': ['s', 'f'],
            'level': logging.DEBUG,
        },
    ))

    return logging.getLogger()


# ON MODULE LOAD
logger = _initialize_logger(Path(__file__).parent / "logs/")


def singleton(collection):
    """
    If the collection has only one distinct element
    return it, otherwise raise an AssertionError
    """
    collection = set(collection)
    assert (1 == len(collection)), F"Series is not a singleton set (len = {len(collection)})"
    return collection.pop()


def assert_str(x: str):
    assert type(x) is str
    return x


def user_loop_choose(choices, callback, sleep=0.5):
    """
    user_loop_chooce(["Coffee", "Tea"], kitchen)
    class kitchen("Coffee") or kitchen("Tea")
    depending on the user input,
    after a delay of `sleep` seconds.
    """
    assert type(choices) in [list, dict]
    choices = pd.Series(choices)
    choices.index = map(str, choices.index)

    while True:
        print("")
        print(choices.to_markdown(headers=''))
        print("")

        try:
            choice = choices[input("Choose one: ")]
        except KeyboardInterrupt:
            print("")
            break
        except:
            break

        print(choice)
        time.sleep(sleep)

        try:
            callback(choice)
        except KeyboardInterrupt:
            continue


class Plot:
    # https://matplotlib.org/3.2.1/tutorials/introductory/customizing.html
    _default_style = {
        'savefig.bbox': "tight",
        'savefig.pad_inches': 0,
        'savefig.transparent': False
    }

    def __init__(self, style=None):
        style = {**self._default_style, **(style or {})}
        try:
            self._style_context = plt.style.context(style)
        except:
            logger.error("Error in Plot initialization")
            raise

    def __enter__(self):
        self._style_context.__enter__()
        (self._f, self._a) = plt.subplots(dpi=180)
        logger.debug(F"Plot: opened figure {self.f}")
        return self

    @property
    def f(self) -> plt.Figure:
        return self._f

    @property
    def a(self) -> plt.Axes:
        return self._a

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug(F"Plot: closing figure {self.f}")
        plt.close(self.f)
        self._style_context.__exit__(exc_type, exc_val, exc_tb)

    @staticmethod
    def show():
        plt.show()


class IdGenerator:
    """
    Generates IDs consisting of a custom prefix,
    a pseudo-unique part, and a sequential part,
    e.g.
        NOOP-FNS7M7E5FD-0001
        NOOP-FNS7M7E5FD-0002
        ...

    The middle part is utcnow in base36
    as of instantiation.

    Usage:
        oig = OrderIdGenerator(prefix="NOOP")
        next(oig)
        next(oig)
    """

    def __init__(self, prefix: str, zfill=4):
        self.prefix = prefix
        self.guid = np.base_repr(round(datetime.utcnow().timestamp() * 1e6), base=36)
        self.count = 0
        self.zfill = zfill

    def __iter__(self):
        return self

    def __next__(self):
        self.count += 1
        return F"{self.prefix}-{self.guid}" + (("-" + str(self.count).zfill(self.zfill)) if self.zfill else "")


def test_orderidgenerator():
    oig = IdGenerator(prefix="NOOP")
    logger.info(next(oig))
    logger.info(next(oig))
    logger.info(next(oig))


if (__name__ == '__main__'):
    # test_response_parser()
    # test_orderidgenerator()
    pass
