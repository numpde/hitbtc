# RA, 2020-05-21

# Estimate potential return under optimal trading

import numpy as np
import pandas as pd
from pathlib import Path
from typing import Collection
from commons import singleton, list_tables
import matplotlib.pyplot as plt

# https://sqlbuilder.readthedocs.io/en/latest/
from sqlbuilder.smartsql import T, Q, F, func as Func

from sqlite3 import connect

PARAM = {
    'db': Path(__file__).parent / "db/trade_history/UV/symbols.db",
    'output': Path(__file__).parent / "db/trade_history/potential0/",

    'sqlite_ext': (Path(__file__).parent / "sqlite/extensions/UV/").glob("*.so"),

    'savefig': dict(bbox_inches='tight', pad_inches=0, transparent=False),
}


def compile_query(q: Q):
    from sqlbuilder.smartsql.dialects.sqlite import compile as compile_sql_
    return dict(zip(['sql', 'params'], compile_sql_(q)))


def minmax_timestamp(con, symbol):
    sql = compile_query(
        Q(T(symbol)).fields(
            Func.MIN(F('timestamp')).as_('min'),
            Func.MAX(F('timestamp')).as_('max'),
        )
    )
    return pd.to_datetime(pd.read_sql_query(con=con, **sql).iloc[0]).dt.to_pydatetime()


def potential_roi(df: pd.DataFrame, side="buy", friction=0.99):
    # Dynamic programming

    (B, S) = ("buy", "sell")

    # Initial investment
    I = side

    order = {B: 0, S: 0}
    tr = {B: S, S: B}

    order[I] = 1

    assert all(df.side.isin(list(order)))
    conversion = df.price ** (1 * (df.side == B) - 1 * (df.side == S))

    for (side, price) in zip(df.side, conversion):
        # An order `row.side` was filled; mimic this order
        order[tr[side]] = max(order[tr[side]], order[side] * price * friction)

    return (order[I] - 1) / 1


def potential0(con, symbol):
    # Process the database month by month
    periods: Collection[pd.Period]
    periods = sorted(set(pd.date_range(*minmax_timestamp(con, symbol), freq='D').to_period('M')))

    period2dt = (lambda p: p.to_timestamp().to_pydatetime())

    frictions = [0.9, 0.95, 0.98, 0.99, 0.995, 0.998, 0.999]

    roi = pd.DataFrame(
        index=periods,
        columns=[],
    )

    for period in periods:
        query = compile_query(
            Q(T(symbol)).fields("*").where(
                (period2dt(period) <= F('timestamp')) & (F('timestamp') < period2dt(period + 1))
            ).order_by(
                F('timestamp').asc()
            )
        )

        df = pd.read_sql_query(con=con, parse_dates=['timestamp'], **query)

        roi.loc[period, 'trades'] = len(df)
        roi.loc[period, 'min_price'] = min(df.price)
        roi.loc[period, 'max_price'] = max(df.price)

        for friction in frictions:
            for side in ["buy", "sell"]:
                roi.loc[period, F"ROI/{side}/{friction}"] = potential_roi(df, side, friction)

        PARAM['output'].mkdir(exist_ok=True, parents=True)

        with (PARAM['output'] / symbol).with_suffix(".txt").open('w') as fd:
            print(roi.to_markdown(), file=fd)

        df.to_csv((PARAM['output'] / symbol).with_suffix(".csv"), sep='\t')


def get_median(con, symbol, column="quantity"):
    query = compile_query(Q(T(symbol)).fields(Func.median(F(column))))
    df = pd.read_sql_query(con=con, parse_dates=['timestamp'], **query)
    return df.squeeze()


def main():
    with connect(PARAM['db']) as con:
        for ext in PARAM['sqlite_ext']:
            con.load_extension(str(ext))

        for symbol in list_tables(con).name:
            print(get_median(con, symbol))
            # potential1(con, symbol)


if __name__ == '__main__':
    main()
