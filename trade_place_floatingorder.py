# RA, 2020-06-05

"""
Place and track an order somewhat off
the bid-ask gap
"""
from contextlib import contextmanager

from commons import logger, IdGenerator
from commons import user_loop_choose

from hitbtc_response_specs import Side

from hitbtc_utils import Session, OrderType, TimeInForce, API_URLS
from hitbtc_utils import get_order_book, get_tickers, create_order_json, get_symbol_info
from hitbtc_utils import place_order as api_place_order

import time
import pandas as pd
import requests

from decimal import Decimal
from datetime import datetime, timedelta


def actio(symbol: str, side: str, session: Session):
    symbol_info = get_symbol_info(symbol)
    logger.debug(F"Symbol info: {symbol_info.to_dict()}")

    side = Side.parse(side)
    logger.debug(F"Side: {side}")

    bonus = Decimal('0.0005')
    logger.debug(F"Aim for price bonus: {bonus}")

    # Helper to round to valid price
    round_price = (lambda p: (p // symbol_info.tickSize) * symbol_info.tickSize)

    # Minimal trading quantity
    quantity = symbol_info.quantityIncrement

    # Price with a bonus as fraction of the current price
    def floating_price():
        tickers = get_tickers(symbol, session)
        (b, a, t) = (tickers.bid, tickers.ask, tickers.timestamp)
        logger.debug(F"Tickers: bid = {b}, ask = {a} at {t}")
        pricer = {
            Side.Buy: min(a, b) / (1 + bonus),
            Side.Sell: max(a, b) * (1 + bonus),
        }
        return round_price(pricer[side])

    logger.debug(F"Target price: {floating_price()}")

    order_id_generator = IdGenerator(prefix="FO01")

    adjustment_freq = timedelta(seconds=6)

    def place_order(order_id):
        try:
            order = create_order_json(
                symbol, side, OrderType.Limit, TimeInForce.GoodTillDate,
                quantity=quantity, price=floating_price(),
                # Preferentially delete the order within `adjustment_freq`, or let it expire
                expire_time=(datetime.utcnow() + adjustment_freq + timedelta(seconds=2)),
                client_order_id=order_id,
            )
            answer = api_place_order(order, session)
            logger.info(F"Initial order placed successfully with answer: \n{answer.to_dict()}")
            return True
        except requests.exceptions.HTTPError:
            df = session.df
            logger.warning(F"Failed to place order. Session last data: \n{df.error}")
            return False

    def cancel_order(order_id):
        try:
            logger.info(F"Deleting order {order_id}")
            session.delete(API_URLS['delete order by id'].format(clientOrderId=order_id))
            logger.info(F"Delete request successfully sent with answer: \n{session.df.to_dict()}")
            return True
        except requests.exceptions.HTTPError:
            logger.warning(F"Failed to delete order {order_id}. Session data: \n{session.df.to_dict()}")
            # 20002 is code for "Order not found", which is OK
            return session.df.error['code'] in [20002]

    order_status = dict()

    def cancel_orders(ids):
        for order_id in ids:
            if order_id in order_status:
                if order_status[order_id]:
                    order_status[order_id] = not cancel_order(order_id)

    try:
        while True:
            order_id = next(order_id_generator)
            order_status[order_id] = place_order(order_id)
            cancel_orders(set(order_status) - {order_id})

            logger.info(F"Order status: \n{order_status}")
            time.sleep(5)
    except KeyboardInterrupt:
        cancel_orders(order_status)
        return


def choose_side(symbol: str):
    with Session(private=True) as session:
        user_loop_choose({'B': "Buy", 'S': "Sell"}, (lambda side: actio(symbol, side, session)), sleep=0)


def main():
    # from commons_db import Historical
    # choose_side("BTCUSD")
    choose_side("ETHUSD")
    # user_loop_choose(Historical.symbols(), choose_side)


if __name__ == '__main__':
    main()
