# RA,2020-06-04

"""
Histograms of swings/returns
"""

from commons import logger, Plot
from commons_db import Historical

import numpy as np
import pandas as pd

from decimal import Decimal
from contextlib import contextmanager
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict

from inclusive import range

PARAM = {
    'output dir': (Path(__file__).parent / "db/trade_history/swings/"),
}


def roi_on_median(price: pd.Series, freq: timedelta) -> np.array:
    price: pd.Series
    price = price.groupby(price.index.round(freq)).median()
    tt = pd.date_range(start=min(price.index), end=max(price.index), freq=freq)
    price = price[tt]
    # price = price.fillna(method='ffill')
    roi = np.exp(np.diff(np.log(price))) - 1
    return roi


def main():
    SEC = timedelta(seconds=1)
    symbols = Historical.symbols()
    period_format = "%Y=%W"
    for symbol in symbols:
        h = Historical(symbol=symbol, period_format=period_format, use_api_types=False)
        for (period, df) in h.items():
            price = df[['timestamp', 'price']].set_index('timestamp').price.sort_index()

            roi_by_freq: Dict[str, np.ndarray]

            # RoI for different windows
            roi_by_freq = {
                freq: roi_on_median(price, (SEC * {'1S': 1, '1M': 60, '1H': 3600}[freq]))
                for freq in ["1S", "1M", "1H"]
            }

            # Count NaN
            nan_by_freq = {
                freq: np.sum(np.isnan(roi)) / len(roi)
                for (freq, roi) in roi_by_freq.items()
            }

            # Percentiles
            # Postulate that NaN means "no change", hence RoI = 0
            qbins_by_freq = pd.DataFrame(data={
                freq: np.percentile(np.nan_to_num(roi, nan=0), q=list(range[0, 100]))
                for (freq, roi) in roi_by_freq.items()
            })

            # Remove NaN
            roi_by_freq = {
                freq: roi[~np.isnan(roi)]
                for (freq, roi) in roi_by_freq.items()
            }

            file_dir = PARAM['output dir'] / F"{symbol}"
            file_dir.mkdir(parents=True, exist_ok=True)

            with (file_dir / F"{period}").with_suffix(".csv").open('w') as fd:
                qbins_by_freq.to_csv(fd, sep='\t')

            for (freq, roi) in roi_by_freq.items():
                plot_style = {'xtick.labelsize': "xx-small"}
                with Plot(plot_style) as p:
                    label = F"#NaN omitted: {round(Decimal(nan_by_freq[freq] * 100), 2)}%"
                    p.a.hist(roi * 100, bins='stone', density=True, label=label)
                    p.a.set_yticks([])
                    p.a.set_xlabel(F"{freq} return, %")
                    xlim = {"1S": (-1, 1), "1M": (-1, 1), "1H": (-10, 10)}[freq]
                    p.a.set_xlim(*xlim)
                    p.a.legend(loc='upper left')
                    p.f.savefig((file_dir / F"{period}_{freq}").with_suffix(F".png"))


if __name__ == '__main__':
    main()
