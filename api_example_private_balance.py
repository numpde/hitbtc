# RA, 2020-05-09

# Example: Retrieve the balance

# https://pypi.org/project/python-dotenv/
from dotenv import load_dotenv
load_dotenv()

import os
KEYS = tuple(map(os.getenv, ("API_KEY", "SECRET_KEY")))

import pandas as pd

# https://api.hitbtc.com/#authentication
import requests
with requests.session() as session:
	session.auth = KEYS
	data = session.get('https://api.hitbtc.com/api/2/trading/balance').json()

# `data` is a list of items like
# 	{'currency': 'DDF', 'available': '0', 'reserved': '0'}
# where
# 	currency	String	Currency code
# 	available	Number	Amount available for trading or transfer to main account
# 	reserved	Number	Amount reserved for active orders or incomplete transfers to main account

column_types = {
	'currency': str,
	'available': float,
	'reserved': float,
}

df: pd.DataFrame
df = pd.DataFrame(data=data).astype(column_types).set_index('currency')
assert (df['available'].dtype == float)
assert (df['reserved'].dtype == float)


df = df.sort_values('available', ascending=False)
print(df[df.any(axis=1)].to_markdown())

# Example output:
# | currency   |   available |   reserved |
# |:-----------|------------:|-----------:|
# | ETH        |   0.1       |          0 |
# | BTC        |   0.0029826 |          0 |
