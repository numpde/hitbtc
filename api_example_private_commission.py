# RA, 2020-05-09

# Get personal trading commission rate
# https://api.hitbtc.com/#get-trading-commission

from commons import logger
from commons import API_URLS, Session


def get_user_commission(symbol):
    with Session(private=True) as session:
        session.get(API_URLS['user commission'].format(symbol=symbol))
        return session.df


def main():
    symbol = "ETHBTC"
    rates = get_user_commission(symbol)
    logger.info(F"The commission for {symbol} is:")
    logger.info(rates)


if __name__ == '__main__':
    main()
