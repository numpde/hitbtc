# RA, 2020-05-28

"""
Question: how long till price rises / falls by a given factor?
"""

from datetime import timedelta

from os import nice as ill_be_nicer

import numpy as np
import pandas as pd

from pathlib import Path

from commons import logger, Plot
from commons_db import Historical

from progressbar import progressbar

PARAM = {
    'save_to': Path(__file__).parent / "db/trade_history/timetohit/",
}


def timetohit(prices: pd.Series, timestamps: pd.Series, jump: float):
    """
    In seconds
    """

    assert jump
    # assert (list(zip(prices, timestamps)) == sorted(list(zip(prices, timestamps))))

    if (jump < 0):
        prices = -prices

    ps = zip(prices, timestamps)
    qt = list(zip(prices, timestamps))

    j = 0

    for (p, s) in ps:
        try:
            k = j
            (q, t) = qt[k]
            while not ((q >= p * (1 + jump)) and (t > s)):
                k += 1
                (q, t) = qt[k]

            j += 1
        except (StopIteration, IndexError):
            yield None
        else:
            yield (t - s).total_seconds()

    for __ in ps:
        yield None


def test_timetohit():
    import numpy as np
    np.random.seed(42)

    N = 100

    data = pd.DataFrame({
        # Time
        't': pd.to_datetime(1e9 * np.cumsum(np.random.rand(N))),
        # Stochastic process
        'x': np.exp(np.cumsum(np.random.randn(N) / 15)),
    })

    # first_t = [
    #     np.min(
    #         # First time a predicate is met later
    #         data.t[(data.x >= 1.01 * x) & (data.t > t)]
    #     )
    #     for (t, x) in zip(data.t, data.x)
    # ]
    #
    # print(sum(np.isnan(first_t)))

    f = 0.01

    tth = list(timetohit(data.x, data.t, f))
    tth = pd.to_timedelta(tth, unit='s')

    print(data.t)
    print(tth)

    with Plot() as p:
        p.a.plot(data.t, data.x, '.-', lw=0.3)

        for (t, dt, x) in zip(data.t, tth, data.x):
            if not pd.isna(dt):
                p.a.plot([t, t + dt], [x, x * (1 + f)], 'r.-', lw=0.3)

        p.a.set_yscale('log')
        p.show()
        input()

    exit()


def main():
    for symbol in Historical.symbols():
        h = Historical(symbol=symbol, period_format="alltime", use_api_types=False)
        trades: pd.DataFrame
        for (period, trades) in h.items():
            trades = trades[trades.quantity >= np.median(trades.quantity)]
            logger.debug(F"Removed lower half by 'quantity'")

            trades = trades[['price', 'timestamp']]

            # # Required for `timetohit`
            # trades = trades.sort_values(by=['price', 'timestamp'])

            for jump_mag in ['0.005', '0.01']:
                for jump_sgn in "+-":
                    jump = jump_sgn + jump_mag
                    logger.debug(F"{symbol}/{period}/{jump}")
                    trades[jump_sgn] = list(timetohit(trades.price, trades.timestamp, jump=float(jump)))

                filepath = PARAM['save_to'] / F"{symbol}/UV/{period}_{jump_mag}.csv"
                filepath.parent.mkdir(parents=True, exist_ok=True)

                trades.to_csv(filepath, sep='\t', index=False)


if __name__ == '__main__':
    ill_be_nicer(19)
    test_timetohit()
    main()
