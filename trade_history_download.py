# RA, 2020-05-10
from enum import Enum

# Collect trade history back in time

import pandas as pd
import sqlite3
from retry import retry

from datetime import datetime, timedelta

import requests
import time

from pathlib import Path


# from collections import namedtuple
# UnitOfWork = namedtuple('UnitOfWork', ['symbol', 'ta', 'tb'])

class Term(Enum):
    LONG_TERM = "trade_history"
    SHORT_TERM = "trade_history_short"


term = Term.SHORT_TERM

PARAM = {
    'db': Path(__file__).parent / f"db/{term.value}/UV/symbols.db",

    # Begin to download
    # 'ta': datetime(year=2022, month=4, day=1, hour=0),
    'ta': None,  # 'None' means take the last available from the DB
}


def now():
    from datetime import timezone, datetime
    # return datetime.now(tz=timezone.utc)
    return datetime.utcnow()


if PARAM['ta'] is None:
    if term == Term.SHORT_TERM:
        # from the beginning of last week (Monday)
        PARAM['ta'] = (
                datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0) -
                timedelta(days=now().weekday(), weeks=1)
        ).replace(
            tzinfo=None
        )

Path(PARAM['db']).parent.mkdir(exist_ok=True, parents=True)

symbols = {
    "BTCUSD",
    "ETHBTC",
    "ETHUSD",
    # "ETHUSDC",
}

column_types = {
    'id': int,
    'price': float,
    'quantity': float,
    'side': str,
    'timestamp': 'datetime64[ms]',  # equivalent to pd.to_datetime
}


@retry(requests.exceptions.RequestException, tries=10, delay=1)
def get_trades(symbol: str, ta: datetime, tb: datetime, session) -> pd.DataFrame:
    # https://api.hitbtc.com/#trades
    # Max value for `limit` is 1000
    limit = 999
    url = "https://api.hitbtc.com/api/2/public/trades?symbols={symbols}&limit={limit}&from={ta}&till={tb}"
    url = url.format(symbols=symbol, limit=limit, ta=ta.isoformat(), tb=tb.isoformat())

    time.sleep(0.5)
    response = session.get(url)

    data = response.json()
    response.raise_for_status()

    if symbol not in data:
        raise RuntimeWarning(F"No '{symbol}' key in response")

    data = data[symbol]

    if (len(data) >= limit) and ((tb - ta) > timedelta(seconds=1)):
        tm = ta + (tb - ta) / 2
        df = pd.concat([get_trades(symbol, ta=ta, tb=tm, session=session),
                        get_trades(symbol, ta=tm, tb=tb, session=session)
                        ])
    else:
        if (len(data) >= limit):
            print("Warning: Could not resolve time window")

        df = pd.DataFrame(data=data)

        if 'timestamp' in df.columns:
            df['timestamp'] = pd.to_datetime(df['timestamp']).dt.tz_localize(None)

        df = (df.astype(column_types).set_index('id') if len(df) else df)

    return df


def download():
    for symbol in symbols:
        with sqlite3.connect(PARAM['db']) as con:
            ta: datetime

            try:
                ta1 = pd.read_sql_query(con=con, sql=F"""select max(timestamp) from {symbol}""").squeeze()
            except:
                ta1 = None

            ta2 = PARAM.get('ta')

            if ta1 and ta2:
                def ask():
                    while True:
                        x = input(f"Symbol: {symbol}. Which timestamp to start from? (1) `{ta1}` or (2) `{ta2}` -- ")
                        if x == "1":
                            return ta1
                        if x == "2":
                            return ta2

                ta = ask()
            else:
                ta = ta1 or ta2

            ta = pd.Timestamp(ta).floor("1min").to_pydatetime()
            tw = timedelta(minutes=4)

            with requests.session() as session:
                while (ta < now()):
                    print(F"Symbol: {symbol}. Timestamp: {ta}")

                    try:
                        trades = get_trades(symbol=symbol, ta=ta, tb=(ta + tw), session=session)

                    except RuntimeWarning:
                        continue
                    finally:
                        ta += tw

                    @retry(tries=4, delay=1)
                    def save_to_db():
                        trades.to_sql(name=symbol, con=con, if_exists='append', index_label="id")

                        # Note: SQLite does not support
                        # con.execute(F"ALTER TABLE `{symbol}` ADD PRIMARY KEY (`id`);")
                        pass

                    save_to_db()

                    # "For [..] Trading history, the limit is 10 requests per second for one user"
                    time.sleep(0.1)


if __name__ == '__main__':
    download()
