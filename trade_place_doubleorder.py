# RA, 2020-05-25

"""
Place a buy-sell order pair at a small price interval
https://api.hitbtc.com/#create-new-order
"""

"""
Note on fees:

Charged fee is determined in symbol's feeCurrency. 
Maker-taker fees offer a transaction rebate provideLiquidityRate 
to those who provide liquidity (the market maker), 
while charging customers who take that liquidity takeLiquidityRate.

To create buy orders, you must have sufficient balance including fees.
Available balance > price * quantity * (1 + takeLiquidityRate)
"""

# TODO:
# Get personal commission:
# https://api.hitbtc.com/#get-trading-commission

from commons import logger
from commons import assert_str
from commons import IdGenerator

from hitbtc_utils import Session
from hitbtc_utils import OrderExpiredError
from hitbtc_utils import Side, TimeInForce, OrderType
from hitbtc_utils import get_symbol_info, get_user_commission
from hitbtc_utils import get_tickers, get_user_balance, get_recent_trades
from hitbtc_utils import create_order_json, place_order

import time
import numpy as np
import pandas as pd

from decimal import Decimal
from datetime import datetime, timedelta, timezone


def propose_orders(symbol, side_first_order: Side, session: Session):
    # https://api.hitbtc.com/#create-new-order

    assert session

    # "Uniqueness must be guaranteed within a single trading day, including all active orders."
    client_order_id = IdGenerator(prefix="DO-02", zfill=2)

    #
    symbol_info = get_symbol_info().T[symbol]
    logger.debug(F"Symbol info: {symbol_info.to_dict()}")

    # baseCurrency: Name (code) of base currency, e.g. BTC
    # quoteCurrency: Name of quote currency, e.g. USD
    # quantityIncrement: Symbol quantity should be divided by this value with no remainder
    # tickSize: Symbol price should be divided by this value with no remainder
    # feeCurrency: Value of charged fee (e.g. USD)

    # Base and quote currencies
    bccy = assert_str(symbol_info.baseCurrency)
    qccy = assert_str(symbol_info.quoteCurrency)

    # TODO: only bccy and qccy
    funds = get_user_balance(session=session)
    funds = {
        bccy: Decimal(funds[symbol_info.baseCurrency].available),
        qccy: Decimal(funds[symbol_info.quoteCurrency].available),
    }
    # E.g.: {'BTC': 0.014380775142, 'USD': 2.7206763392}
    logger.info(F"Funds for symbol {symbol}: {funds[bccy]} {bccy}, {funds[qccy]} {qccy}")

    # # Recent transactions
    # recent = get_recent_trades(symbol)
    # assert len(recent)

    #
    tickers = get_tickers(symbol, session=session)

    if not (tickers.bid and tickers.ask):
        raise RuntimeError(F"Bid = {tickers.bid}, ask = {tickers.ask}")

    # side_for_price_quote = Side.other(side_first_order)

    # # Recent filled order of other order type (if any)
    # if side_for_price_quote in set(recent.side):
    #     recent = recent[recent.side == side_for_price_quote]
    #     logger.debug(F"Price suggestion from recent '{side_for_price_quote}' transactions")
    # else:
    #     logger.warning(F"No '{side_first_order}' in recent transactions")
    #
    # recent = recent.iloc[0]
    #
    # ago = datetime.now(tz=timezone.utc) - recent.timestamp.to_pydatetime()
    # logger.info(F"Last {recent.side} price: {recent.price} {qccy} / 1 {bccy} ({ago.total_seconds():.3} seconds ago)")

    # # Total funds for transactions with this symbol
    # total_funds_est_bccy = funds[bccy] + funds[qccy] / last_price
    # total_funds_est_qccy = funds[qccy] + funds[bccy] * last_price
    # logger.debug(F"Total liquid funds in base ccy  ({bccy}): {total_funds_est_bccy}")
    # logger.debug(F"Total liquid funds in quote ccy ({qccy}): {total_funds_est_qccy}")

    # Reference price to buy/sell
    price0 = (tickers.bid + tickers.ask) / 2

    expire_time = datetime.utcnow() + timedelta(days=7)

    # Note: at maker fee of 0.001, need at least 0.002 premium
    premiums = [Decimal('0'), Decimal('0.004')]
    sides = [side_first_order, Side.other(side_first_order)]

    orders = []

    quantity = Decimal(0)

    # Only use this much of available funds
    # Increase a little to at least hit minimal trading quantity
    for funds_take_factor in (Decimal('0.1') * np.arange(1, 11)):
        quantity = min(funds[bccy], funds[qccy] / price0) * funds_take_factor
        quantity = (quantity // symbol_info.quantityIncrement) * symbol_info.quantityIncrement

        if quantity:
            break

    if (quantity == 0):
        raise RuntimeError("Not enough funds to initiate double-transaction")

    for (side, premium) in zip(sides, premiums):
        price = price0 * ((1 + premium) if (side == Side.Sell) else (1 - premium))
        price = (price // symbol_info.tickSize) * symbol_info.tickSize

        (give_ccy, take_ccy) = ((bccy, qccy) if (side == Side.Sell) else (qccy, bccy))
        (give_amt, take_amt) = ((quantity, quantity * price) if (side == Side.Sell) else (quantity * price, quantity))

        if (funds[give_ccy] < give_amt):
            what = F"Insufficient funds to {side.value} {quantity} {bccy} at price {price} (only have {funds[give_ccy]} {give_ccy})"
            logger.warning(what)
            raise RuntimeError(what)

        order = create_order_json(
            symbol, side, OrderType.Limit,
            TimeInForce.GoodTillDate,
            quantity=quantity, price=price,
            expire_time=expire_time,
            client_order_id=next(client_order_id),
        )

        orders.append(order)

    return orders


def place_orders(orders, session: Session):
    answers = []

    for order in orders:
        answer = place_order(order, session)
        answers.append(answer)

        order_status = answer['status']

        if (order_status == 'new'):
            pass

        if (order_status == 'expired'):
            logger.warning(F"Aborting placing orders!!!")
            raise OrderExpiredError


def user_loop(symbol):
    # Base and quote currencies
    symbol_info = get_symbol_info().T[symbol]
    bccy = assert_str(symbol_info.baseCurrency)
    qccy = assert_str(symbol_info.quoteCurrency)
    fccy = assert_str(symbol_info.feeCurrency)

    comm_maker = get_user_commission(symbol).provideLiquidityRate
    comm_taker = get_user_commission(symbol).takeLiquidityRate

    assert (qccy == fccy)

    with Session(private=True) as session:
        while True:
            recent = get_recent_trades(symbol, session)
            now = datetime.now(tz=timezone.utc)
            recent['ago(s)'] = recent['timestamp'].apply(lambda x: (now - x.to_pydatetime()).total_seconds())
            recent = recent.drop(columns=['timestamp'])
            logger.info(recent.head())

            time.sleep(0.1)

            tentative_orders = {
                side_first_order:
                    propose_orders(symbol, side_first_order=side_first_order, session=session)
                for side_first_order in [Side.Buy, Side.Sell]
            }

            for (sfo, (o1, o2)) in tentative_orders.items():
                logger.info(F"Tentative orders for {sfo.value} first:")
                logger.info(F"\t{o1['side']: <5} {o1['quantity']} {bccy} at {o1['price']} {qccy} / 1 {bccy}")
                logger.info(F"\t{o2['side']: <5} {o2['quantity']} {bccy} at {o2['price']} {qccy} / 1 {bccy}")

                txn = {
                    o1['side']: Decimal(o1['quantity']) * Decimal(o1['price']),
                    o2['side']: Decimal(o2['quantity']) * Decimal(o2['price']),
                }

                # Note: use comm_make fee because orders are post-only
                (delta, c1, c2) = (txn['sell'] - txn['buy'], txn[o1['side']] * comm_maker, txn[o2['side']] * comm_maker)
                logger.info(F"\tMake {delta} {qccy}")
                logger.info(F"\t  -  {c1} {qccy}")
                logger.info(F"\t  -  {c2} {qccy}")
                logger.info(F"\t  =  {delta - c1 - c2} {qccy}")

            side = input(F"Place a double-order on {symbol}. [B]uy or [S]ell? \n").upper()
            logger.debug(F"User input: {side}")

            if not side in ["B", "S", "V"]:
                print("Not understood")
                continue

            if side in ["B", "S"]:
                try:
                    side_first_order = (Side.Buy if side.startswith("B") else Side.Sell)
                    orders = propose_orders(symbol, side_first_order=side_first_order, session=session)
                    place_orders(orders, session)
                except OrderExpiredError:
                    logger.warning(F"Sorry, order(s) expired")

            if side in ["V"]:
                for side_first_order in [Side.Buy, Side.Sell]:
                    orders = propose_orders(symbol, side_first_order=side_first_order, session=session)
                    place_orders(orders[1:], session)


def run_sanity_checks(symbol):
    logger.info("Sanity checks...")
    with Session(private=True) as session:
        symbols = get_symbol_info()
        recent = get_recent_trades(symbol, session)
        order = create_order_json(symbol, Side.Buy, OrderType.Limit, TimeInForce.FillOrKill, quantity=0, price=1)

        balance = get_user_balance(session)
        orders = propose_orders(symbol, side_first_order=Side.Buy, session=session)

    logger.debug("Sanity checks: Finished")


def main():
    from commons import user_loop_choose
    # from commons_db import Historical
    symbols = ["ETHUSD", "BTCETH"]  # Historical.symbols()
    user_loop_choose(sorted(symbols), user_loop)


if __name__ == '__main__':
    main()
