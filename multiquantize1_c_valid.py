# RA, 2020-06-06

from commons import logger
from commons import Plot

import os
import numpy as np
import pandas as pd
import pickle
import sklearn

from pathlib import Path

# Required by the model
from multiquantize1_b_train import preprocess

ROOT_PATH = Path(__file__).parent


def load_model():
    filepath = sorted((ROOT_PATH / "db/trade_history/multiquantize/models/UV/").glob("*.pkl"))[-1]
    logger.debug(F"Loading model from: {os.path.relpath(filepath, ROOT_PATH)}")

    with filepath.open(mode='rb') as fd:
        model_with_meta = pickle.load(fd)

    model = model_with_meta['model']

    # logger.debug(F"Model: {model}")
    return model


def main():
    from multiquantize1_b_train import load_dataset_by_period

    (X, y, ds_meta) = load_dataset_by_period(symbol="BTCUSD", period="2020-06-04")
    logger.debug(F"Validation dataset: {ds_meta.to_dict()}")

    model = load_model()

    y_pred = model.predict(X)
    logger.debug(F"Predictions shape: {y_pred.shape}")

    import matplotlib.pyplot as plt
    with Plot() as p:
        p.a.scatter(y, y_pred, s=1)
        p.a.grid()
        p.show()


if __name__ == '__main__':
    main()
