# RA, 2020-05-28

"""
Convenience functions for common DB queries

Note:
Uses @dataclass, so requires python>=3.7
https://www.python.org/dev/peps/pep-0557/
"""

import logging as logger

try:
    from commons import logger
except ModuleNotFoundError as ex:
    logger.warning(F"Module not found: {ex}")

import numpy as np
import pandas as pd
import sqlite3

from retry import retry
from typing import List, Tuple, Iterable
from pathlib import Path
from datetime import datetime, timedelta, timezone
from functools import lru_cache as in_memory_cache
from dataclasses import dataclass
from sqlbuilder.smartsql import Q, T, F, func as Func


def list_tables(con: sqlite3.Connection) -> pd.DataFrame:
    return pd.read_sql_query("SELECT * FROM sqlite_master WHERE type='table'", con)


DB_PATH = {
    'trade history': Path(__file__).parent / "db/trade_history/UV/symbols.db",
}

if 'copy' in str(DB_PATH['trade history']):
    logger.warning(F"Using DB copy {DB_PATH['trade history']}")


@in_memory_cache
def compile_sql(q: Q):
    """
    Returns a dictionary
        dict(sql=..., params=...)
    """
    from sqlbuilder.smartsql.dialects.sqlite import compile as compile_sql_
    query = dict(zip(['sql', 'params'], compile_sql_(q)))
    logger.debug(F"Compiled query: {query}")
    return query


@dataclass
class Historical:
    """
    Example:
         df = Historical(symbol="BTCUSD", period_format="%Y-%m", use_api_types=False)["2020-05"]
    Placeholders:
        %d		day of month: 00
        %f		fractional seconds: SS.SSS
        %H		hour: 00-24
        %j		day of year: 001-366
        %J		Julian day number
        %m		month: 01-12
        %M		minute: 00-59
        %s		seconds since 1970-01-01
        %S		seconds: 00-59
        %w		day of week 0-6 with Sunday==0
        %W		week of year: 00-53
        %Y		year: 0000-9999
        %%		%
    See:
        https://www.sqlite.org/lang_datefunc.html
    """

    symbol: str
    period_format: str = "anytime"
    use_api_types: bool = False
    datetime_field: str = "timestamp"

    @classmethod
    @retry(sqlite3.OperationalError, tries=3, delay=0.2)
    def symbols(cls) -> List[str]:
        with sqlite3.connect(DB_PATH['trade history']) as con:
            return sorted(list_tables(con).name)

    @retry(sqlite3.OperationalError, tries=3, delay=0.2)
    def execute(self, query: Q) -> pd.DataFrame:
        with sqlite3.connect(DB_PATH['trade history']) as con:
            return pd.read_sql_query(con=con, **compile_sql(query), parse_dates=self.datetime_field)

    # @in_memory_cache  # Unhashable type `Historical`
    def keys(self) -> Iterable[str]:
        # List periods matching `datetime_format`
        query = Q(T(self.symbol)).fields(
            Func.strftime(self.period_format, F(self.datetime_field)).distinct().as_("period")
        ).order_by(
            F("period")
        )
        periods = self.execute(query).period
        # logger.debug(F"periods: {periods}")
        yield from periods

    def values(self) -> Iterable[pd.DataFrame]:
        for k in self.keys():
            yield self[k]

    def items(self) -> Iterable[Tuple[str, pd.DataFrame]]:
        for k in self.keys():
            yield (k, self[k])

    def __getitem__(self, period: str) -> pd.DataFrame:
        assert self.symbol
        assert self.period_format
        assert self.datetime_field

        # # Sanity check: period compatible with the filter datetime format
        # # Doesn't work when self.datetime_format is any string to indicate "alltime"
        # assert datetime.strptime(period, self.datetime_format)

        query = Q(T(self.symbol)).fields("*").where(
            (Func.strftime(self.period_format, F(self.datetime_field)) == period)
        ).order_by(
            F(self.datetime_field)
        )

        df = self.execute(query)

        if self.use_api_types:
            try:
                from hitbtc_utils import ResponseSpecs
            except ModuleNotFoundError as ex:
                logger.warning(F"Module not found: {ex}")
            else:
                df = ResponseSpecs.TradesBySymbol.Data.apply(df)

        return df
