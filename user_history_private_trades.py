# RA, 2020-05-25

"""
Retrieve own trade history
"""


from pathlib import Path
from sqlite3 import connect

import pandas as pd

from commons import logger
from commons import Side
from commons import API_URLS, Session

PARAM = {
    'user history db': Path(__file__).parent / "db/user_history/UV/user_history.db",

    'tbl': "user_trades",
}


def get_user_trades():
    df: pd.DataFrame

    with Session(private=True) as session:
        session.get(API_URLS['user trades'].format(symbol=""))
        df = session.df
        # logger.debug(df.to_markdown())

    db_path = PARAM['user history db']
    db_path.parent.mkdir(parents=True, exist_ok=True)

    logger.debug(F"Got df with columns {list(df.columns)}")

    df = df.set_index('id')

    # Convert offensive datatypes to str
    df['quantity'] = df['quantity'].apply(str)
    df['price'] = df['price'].apply(str)
    df['side'] = df['side'].apply(Side.unparse)
    df['fee'] = df['fee'].apply(str)

    with connect(db_path) as con:
        tbl_name = PARAM['tbl']

        # Dump to DB
        df.to_sql(con=con, name=tbl_name, if_exists="append")

        # Deduplicate (SQLite)
        sql = F"""
            delete from {tbl_name}
            where rowid not in (
                select min(rowid) from {tbl_name} group by `id`
            )
        """
        con.execute(sql)


def main():
    get_user_trades()


if __name__ == '__main__':
    main()
