# RA, 2020-06-05

"""
The ARIMA model
Template
"""

from commons_db import Historical
from commons import logger

import time
import pandas as pd

from datetime import datetime, timedelta
from statsmodels.tsa.arima_model import ARIMA

PARAM = {

}


def arima(price: pd.Series):
    logger.debug(F"ARIMA on series: \n{price}")
    model = ARIMA(price, order=(5, 1, 0), )
    model_fit = model.fit(disp=2)
    logger.debug(F"ARIMA summary: \n{model_fit.summary()}")
    time.sleep(2)


def prepricess(price: pd.Series, timestep=timedelta(seconds=60), ffill=True) -> pd.Series:
    """
    Assumes `price` is indexed by timestamp/datetime.
    Round the timestamp to multiples of `timestep`,
    lumping the price to its bucket-median.
    Forward-fill missing values if `ffill` is true.
    """
    # Put sampling on a regular grid
    price: pd.Series
    price = price.groupby(price.index.round(timestep)).median()
    assert not any(price.isna())
    # Equidistant sampling
    tt = pd.date_range(start=min(price.index), end=max(price.index), freq=timestep)
    price = price[tt]
    price.index = tt
    # Forward-fill missing values
    price = price.ffill() if ffill else price
    return price


def main():
    symbol = "BTCUSD"
    h = Historical(symbol, "%Y-%m-%d", use_api_types=False)
    for (period, df) in h.items():
        price = prepricess(df.set_index('timestamp').price)
        arima(price)


if __name__ == '__main__':
    main()
