# RA, 2020-05-31

"""
Annotate timepoints by 'urgency to buy'
"""

import numpy as np
import pandas as pd

from pathlib import Path
from datetime import timedelta
from contextlib import contextmanager

from commons import logger, Plot
from commons_db import Historical

from algorithms import windowed_forward_max

PARAM = {
    'output': Path(__file__).parent / "db/trade_history/hotpoint/",
}


def urgency(price: pd.Series, window: timedelta) -> pd.Series:
    window_return = windowed_forward_max(price, window) / price - 1
    return window_return


@contextmanager
def plot_urgency(df: pd.DataFrame) -> Plot:
    df['timestamp'] = df.timestamp.dt.round('1S')

    price = df[['timestamp', 'price']].groupby('timestamp').median().price

    uu = urgency(price, window=timedelta(minutes=1))
    # uu = (uu - min(uu)) / (max(uu) - min(uu))

    from matplotlib.colors import LinearSegmentedColormap, ListedColormap
    cmap = LinearSegmentedColormap.from_list('name', ["red", "green"])
    cmap = cmap(np.arange(cmap.N))
    cmap[:, -1] = np.linspace(0, 1, len(cmap))
    cmap = ListedColormap(cmap)

    plot_style = {
        'xtick.labelsize': "xx-small",
        'ytick.labelsize': "xx-small",
    }

    with Plot(style=plot_style) as p:
        p.a.plot(price.index, price, lw=0.5, c="b", zorder=-100)
        for (n, (i, ui)) in enumerate(uu.groupby(pd.cut(uu, 10))):
            p.a.scatter(
                x=list(ui.index), y=list(price[ui.index]),
                c=list(ui), cmap=cmap,
                marker='o', s=1,
                linewidths=0, edgecolors='none',
                zorder=(n * 100000),
            )
        p.f.autofmt_xdate()
        yield p


def main():
    symbol = "BTCUSD"

    h = Historical(symbol=symbol, period_format="%Y-%m-%d", use_api_types=False)

    for (period, df) in h.items():
        with plot_urgency(df) as p:
            filename = PARAM['output'] / F"{symbol}/{period}.png"
            filename.parent.mkdir(exist_ok=True, parents=True)
            p.f.savefig(filename, dpi=90)
            logger.info(F"Saved figure: {filename}")


if __name__ == '__main__':
    main()
