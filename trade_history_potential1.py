# RA, 2020-05-26

"""
Visualize the 'optimal' trading strategy
"""

import numpy as np
import pandas as pd

from more_itertools import pairwise

from commons import logger, Plot
from commons_db import Historical

import matplotlib.pyplot as plt

from collections import defaultdict
from contextlib import contextmanager

from typing import Dict
from pathlib import Path

PARAM = {
    # 'db': Path(__file__).parent / "db/trade_history/UV/symbols.db",
    'output': Path(__file__).parent / "db/trade_history/potential1/"
}


@contextmanager
def plot_hot_times(my_trades_by_friction: Dict[str, pd.Series]) -> Plot:
    plot_style = {'xtick.labelsize': "xx-small"}
    with Plot(style=plot_style) as p:
        for (label, my_trades) in my_trades_by_friction.items():
            when: pd.Series
            when = pd.Series(my_trades.dropna().index)

            # # Version 1 (use with autofmt_xdate)
            # when = when.dt.round('1H')
            # when = when.dt.time

            # Version 2
            when = when.dt.hour

            # Make histogram
            when = when.value_counts(normalize=True)
            when = when.sort_index()

            p.a.plot(when.index, when, '.--', label=label)

        p.a.legend(loc="upper left")

        # p.f.autofmt_xdate()
        yield p


@contextmanager
def plot_my_trades(my_trades_by_friction: Dict[str, pd.Series], all_trades: pd.DataFrame) -> Plot:
    def config_xaxis_days(ax1):
        # Format the ticks
        # https://matplotlib.org/3.2.1/gallery/text_labels_and_annotations/date.html
        import matplotlib.dates as mdates
        ax1.xaxis_date()
        mj_loc = mdates.DayLocator()
        mj_fmt = mdates.DateFormatter('%Y-%m-%d')
        mi_loc = mdates.HourLocator()
        ax1.xaxis.set_major_formatter(mj_fmt)
        ax1.xaxis.set_major_locator(mj_loc)
        ax1.xaxis.set_minor_locator(mi_loc)

        ax1.get_figure().autofmt_xdate()

    #
    all_trades = all_trades.sort_values('timestamp')

    #
    colors = {'buy': "g", 'sell': "r"}

    plot_style = {'xtick.labelsize': "xx-small"}
    with Plot(style=plot_style) as p:

        for (side, df) in all_trades.groupby(by='side'):
            p.a.plot(df.timestamp, df.price, ls="-", c=colors[side], marker=None, lw=0.5, zorder=100)

        p.a.grid(True)
        config_xaxis_days(p.a)

        ylim = p.a.get_ylim()

        yys = pairwise(np.linspace(*ylim, len(my_trades_by_friction) + 1))
        for (yy, (__, my_trades)) in zip(yys, sorted(my_trades_by_friction.items())):
            for (ts, side) in my_trades.dropna().items():
                p.a.plot([ts, ts], yy, c=colors[side], lw=0.5, alpha=0.5, zorder=-100)

        datemin = pd.to_datetime(all_trades.timestamp.iloc[0]).floor('D')
        datemax = pd.to_datetime(all_trades.timestamp.iloc[-1]).ceil('D')
        p.a.set_xlim(datemin, datemax)
        p.a.set_ylim(*ylim)

        yield p


def simulate_trades(all_trades: pd.DataFrame, friction=0.01) -> pd.Series:
    """
    The dataset `all_trades` should contain the columns `timestamp` and `price`,
    where `price` is in quote currency for a unit of base currency.
    The timestamp will be rounded to 1 second, assigning the median price.
    Based on this timeseries, the optimal trading strategy is computed,
    starting and ending with the base currency.
    The parameter `friction` is the conversion/transaction loss;
    e.g., if friction = 0.01, then 1% is lost at each transaction.
    Returns a series X indexed by the rounded timestamps where
    X[t] is 'buy' or 'sell', referring to the price at time t.
    """

    assert (0 <= friction < 1), "`friction` should be a small non-negative number"

    logger.debug(F"Dynamic programming: forward pass with friction = {friction}...")

    price = all_trades[['timestamp', 'price']].groupby(
        # Quantize time
        all_trades.timestamp.dt.round('1S')
    ).agg(
        # Approximate trading price as the window median
        price=pd.NamedAgg(column='price', aggfunc=np.median),
    ).price

    # Base currency and quote currency columns
    (B, Q) = (0, 1)

    wealth = defaultdict(float)
    backtr = defaultdict(tuple)
    action = defaultdict(str)

    # Begin with an investment in base currency
    wealth[(-1, B)] = 1
    wealth[(-1, Q)] = 0

    # This is the initial state
    backtr[(-1, B)] = None
    backtr[(-1, Q)] = None

    # wealth[(i, *)] is the new state after action at time i

    for i in range(len(price)):
        wealth[(i, B)] = wealth[(i - 1, B)]
        backtr[(i, B)] = (i - 1, B)

        wealth[(i, Q)] = wealth[(i - 1, Q)]
        backtr[(i, Q)] = (i - 1, Q)

        q = wealth[(i - 1, B)] * price.iloc[i] * (1 - friction)
        if (q > wealth[(i, Q)]):
            # Action: sell base currency
            action[(i, Q)] = "sell"
            wealth[(i, Q)] = q
            backtr[(i, Q)] = (i - 1, B)

        b = wealth[(i - 1, Q)] / price.iloc[i] * (1 - friction)
        if (b > wealth[(i, B)]):
            # Action: buy base currency
            action[(i, B)] = "buy"
            wealth[(i, B)] = b
            backtr[(i, B)] = (i - 1, Q)

    # Backtracking
    my_trades = pd.DataFrame(index=price.index, columns=["max bccy", "max qccy"], data=np.nan)

    logger.debug(F"Opening: {wealth[(-1, B)]} [bccy], {wealth[(-1, Q)]} [qccy]")
    logger.debug(F"Closing: {wealth[(len(price) - 1, B)]} [bccy], {wealth[(len(price) - 1, Q)]} [qccy]")

    logger.debug(F"Dynamic programming: backward pass...")

    # C = currency to maximize
    for C in [B, Q]:
        endpt = (len(price) - 1, C)
        while endpt:
            if endpt in action:
                my_trades.iloc[endpt[0], C] = action[endpt]
            endpt = backtr[endpt]

    # logger.debug(F"Dynamic programming result: {my_trades.dropna()}")

    return my_trades.iloc[:, B]


def potential1(symbol):
    h = Historical(symbol=symbol, period_format="%Y-%m", use_api_types=False)

    for (period, all_trades) in h.items():
        my_trades_by_friction = {
            str(friction): simulate_trades(all_trades, friction=friction)
            for friction in [0.048, 0.024, 0.012, 0.006, 0.003]
        }

        out_path = PARAM['output'] / F"{symbol}"
        out_path.mkdir(exist_ok=True, parents=True)

        with plot_my_trades(my_trades_by_friction, all_trades) as p:
            p.f.savefig(out_path / F"protrades_{period}.png")

        with plot_hot_times(my_trades_by_friction) as p:
            p.f.savefig(out_path / F"hot_times_{period}.png")


def main():
    for symbol in Historical.symbols():
        potential1(symbol)


if __name__ == '__main__':
    main()
