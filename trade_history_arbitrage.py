# RA, 2020-05-30

from commons import logger, Plot

from hitbtc_utils import get_symbol_info
from hitbtc_response_specs import Side

import pandas as pd
import numpy as np
import networkx as nx

from dataclasses import dataclass
from typing import List, Tuple
from pathlib import Path

from commons_db import Historical

from inclusive import range


@dataclass
class Link:
    side: Side
    symbol: str
    have: str
    want: str


# cycles = [
#     [
#         Link(Side.Buy, "BTCUSD"),   # USD -> BTC
#         Link(Side.Buy, "ETHBTC"),   # BTC -> ETH
#         Link(Side.Sell, "ETHUSD"),  # ETH -> USD
#     ],
# ]

def construct_ccy_graph(symbols: None) -> nx.DiGraph:
    symbol_info = get_symbol_info()
    # Fields:
    # 'baseCurrency', 'quoteCurrency', 'quantityIncrement', 'tickSize',
    # 'takeLiquidityRate', 'provideLiquidityRate', 'feeCurrency'

    g = nx.DiGraph()

    for (symbol, row) in symbol_info.iterrows():
        if symbols:
            if symbol not in symbols:
                continue

        g.add_edge(
            u_of_edge=row.baseCurrency,
            v_of_edge=row.quoteCurrency,
            symbol=symbol, side=Side.Sell,
        )

        g.add_edge(
            u_of_edge=row.quoteCurrency,
            v_of_edge=row.baseCurrency,
            symbol=symbol, side=Side.Buy,
        )

    return g


def all_cycles(g: nx.DiGraph) -> List[Tuple[Tuple[str, str]]]:
    """
    Enumerate possible trading cycles
    """

    # e2symbol = nx.get_edge_attributes(g, name="symbol")
    # e2side = nx.get_edge_attributes(g, name="side")

    cycles = [
        tuple(
            (str(e[0]), str(e[1]))
            for e in zip(c, [*c[1:], c[0]])
        )
        for c in nx.simple_cycles(g)
    ]

    return cycles


def cycle_roi(rates: dict, cycle: list):
    return np.prod([rates[e] for e in cycle]) - 1


def get_edge_rates(g: nx.DiGraph, prices: dict | pd.Series):
    rates = {}

    for (u, v, edge_data) in g.edges.data():
        sell_price = prices[edge_data['symbol']]
        rates[(u, v)] = sell_price if (edge_data['side'] == Side.Sell) else (1 / sell_price)

    return rates


def cycle_pretty(c):
    return c[0][0] + " -> " + (" -> ".join(str(b) for (a, b) in c))


def arbitrage():
    symbols = Historical.symbols()

    g: nx.DiGraph
    g = construct_ccy_graph(symbols)

    cycles = all_cycles(g)
    cycles = [c for c in cycles if (len(c) >= 3)]

    period_format = "%Y=%W"
    periods = [period_format.replace("%Y", "2022").replace("%W", str(W).zfill(2)) for W in range[15, 20]]

    # period_format = "%Y-%m-%d"
    # periods = ["2022-05-12"]

    logger.debug(F"symbols = {symbols}")
    logger.debug(F"cycles = {[cycle_pretty(c) for c in cycles]}")
    logger.debug(F"periods = {periods}")

    historical = {
        symbol: Historical(symbol=symbol, period_format=period_format, use_api_types=False)
        for symbol in symbols
    }

    def quantize_timestamp(df: pd.DataFrame) -> pd.DataFrame:
        sub_df = df[['timestamp', 'price', 'quantity']]

        agg_df = (
            sub_df
            .assign(cumprice=(sub_df['price'] * sub_df['quantity']))
            .groupby(df['timestamp'].dt.round('60S'))
            .agg(
                cumprice=pd.NamedAgg(column='cumprice', aggfunc='sum'),
                quantity=pd.NamedAgg(column='quantity', aggfunc='sum'),
            )
            .assign(price=(lambda x: x['cumprice'] / x['quantity']))
            [['price', 'quantity']]
        )

        return agg_df

    for period in periods:
        logger.debug(f"period = {period}")

        logger.debug(f"Loading trades...")

        trades = {
            symbol: quantize_timestamp(historical[symbol][period])
            for symbol in symbols
        }

        logger.debug(f"Loading prices...")

        hist_prices = pd.DataFrame({
            symbol: trades[symbol].price
            for symbol in symbols
        })

        # hist_prices = hist_prices.fillna(method='ffill').fillna(method='bfill')

        # Initialize empty dataframe
        hist_arbitrage = pd.DataFrame(
            index=hist_prices.index,
            columns=[cycle_pretty(c) for c in cycles],
        )

        logger.debug(f"Computing cycle costs...")

        # from joblib import Parallel, delayed

        def worker(ts, prices: dict | pd.Series, g: nx.DiGraph):
            rates = get_edge_rates(g=g, prices=prices)
            rois = [cycle_roi(rates=rates, cycle=cycle) for cycle in cycles]
            return (ts, rois)

        results = [worker(ts=ts, prices=prices, g=g) for (ts, prices) in hist_prices.iterrows()]

        for (ts, costs) in results:
            hist_arbitrage.loc[ts] = costs

        # for (ts, prices) in hist_prices.iterrows():
        #     price_by_symbol = {symbol: price for (symbol, price) in prices.items()}
        #     set_edge_rates(g, price_by_symbol)
        #     costs = [(cycle_cost(g, c) - 1) for c in cycles]
        #     hist_arbitrage.loc[ts] = costs
        #     # logger.debug(ts)

        plot_style = {'xtick.labelsize': "xx-small"}

        with Plot(style=plot_style) as p:

            # Plot cyclic arbitrage opportunities

            for cycle in hist_arbitrage:
                arb = hist_arbitrage[cycle].dropna()
                arb = arb[arb >= 0.001]
                p.a.plot(arb, ls='none', marker='.', label=cycle, zorder=100)

            p.a.legend(loc="upper left")

            # Plot prices (standardized)

            ax2 = p.a.twinx()

            for symbol in hist_prices.columns:
                prices = hist_prices[symbol]
                prices = prices.fillna(method='ffill').fillna(method='bfill')
                prices = (prices - prices.mean()) / prices.std()

                [(u, v)] = [
                    (u, v)
                    for (u, v, data) in g.edges.data()
                    if (data['symbol'] == symbol) and (data['side'] == Side.Sell)
                ]

                ax2.plot(prices, ls='-', label=f"{u}/{v}", zorder=-100, marker='none', linewidth=0.5)

            ax2.set_yscale('linear')
            ax2.legend(loc="upper right")

            # # The following makes for a very crowded plot
            # ax2: type(p.a)
            # ax2 = p.a.twinx()
            # ax2.set_yscale('log')
            # for symbol in symbols:
            #     ax2.plot(trades[symbol].quantity, ls='.', label=symbol, zorder=-100)
            # ax2.legend(loc="upper right")

            filename = Path(__file__).parent / F"db/trade_history/arbitrage/figs/{period}"
            filename.parent.mkdir(parents=True, exist_ok=True)
            p.f.savefig(filename.with_suffix(".png"))


def main():
    arbitrage()


if __name__ == '__main__':
    main()
