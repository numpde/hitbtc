# RA, 2020-06-01


import numpy as np
import pandas as pd
from datetime import datetime, timedelta

import logging

logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())


def windowed_forward_max(timeseries: pd.Series, window: timedelta) -> pd.Series:
    """
    Reference implementation:

    ref = pd.Series(
        index=timeseries.index,
        data=[
            np.max(timeseries[(i <= timeseries.index) & (timeseries.index <= (i + window))])
            for i in timeseries.index
        ]
    )
    return ref
    """

    from typing import Deque

    class E:
        def __init__(self, j, q):
            self.j = j
            self.q = q

    class DequeM(Deque[E]):
        def append(self, x: E) -> None:
            while self and (self.front.q <= x.q):
                self.pop()
            return super(DequeM, self).append(x)

        @property
        def max(self):
            return self[0].q

        @property
        def front(self):
            return self[-1]

        @property
        def back(self):
            return self[0]

    deq = DequeM()

    maxed = pd.Series(index=timeseries.index, data=np.nan)

    jq = Deque[E](E(j, q) for (j, q) in timeseries.items())

    deq.append(jq.popleft())
    for (i, p) in timeseries.items():
        try:
            while jq and (jq[0].j < i + window):
                deq.append(jq.popleft())
        except StopIteration:
            pass

        while (deq.back.j < i):
            deq.popleft()

        maxed[i] = deq.max

    return maxed


def test_windowed_forward_max():
    logger.info(F"Running test_windowed_forward_max")
    import pandas as pd
    import numpy as np
    from datetime import datetime, timedelta
    ta = datetime.utcnow()
    tb = ta + timedelta(hours=1)
    N = 256
    timeseries = pd.Series(
        index=pd.date_range(ta, tb, periods=N),
        data=np.random.rand(N),
    ).sample(
        N // 2, replace=True
    ).sort_index()

    window = timedelta(minutes=(1 + np.random.rand()))

    ref = pd.Series(
        index=timeseries.index,
        data=[
            np.max(timeseries[(i <= timeseries.index) & (timeseries.index <= (i + window))])
            for i in timeseries.index
        ]
    )

    df = pd.DataFrame({
        'reference': ref,
        'candidate': windowed_forward_max(timeseries, window),
    })

    assert all(df.reference == df.candidate)
    logger.info("Passed: test_windowed_forward_max")


def multiquantize(price: pd.Series, tb: datetime, nscales=4, nsamples=8, max_resolution=timedelta(seconds=1)):
    """
    Compute a nscales x nsamples array where the first row
    contains values from `price` at frequency `max_resolution`
    leading up to index `t`.
    The next rows each reduce the resolution by a factor of two.
    Returns a tuple (signal, price_t) where
    `signal` is the computed array and
    `price_t` is the relevant subsequence of `price`.
    """

    ta = tb - nsamples * (2 ** (nscales - 1)) * max_resolution
    price_t = price[(ta <= price.index) & (price.index <= tb)]
    del price

    price_n = price_t.copy()
    price_n.index = (price_n.index - tb) / max_resolution
    price_n = price_n[(-nsamples * (2 ** (nscales - 1)) <= price_n.index) & (price_n.index <= 0)]

    signal = np.nan * np.zeros([nscales, nsamples])

    for scale in range(nscales):
        fp = price_n.groupby(np.round(price_n.index / (2 ** scale))).median()
        x = list(range(-nsamples + 1, 1))
        signal[scale] = np.interp(x=x, xp=fp.index, fp=fp, left=np.nan, right=np.nan)

    return (signal, price_t)


def test_multiquantize():
    logger.info(F"Running test_multiquantize")
    max_resolution = timedelta(seconds=1.5)
    scale = 3
    resolution = max_resolution * (2 ** scale)
    nsamples = 5
    t0 = datetime.utcnow()
    tt = list(reversed(-np.arange(0, nsamples) * resolution + t0))
    vv = np.random.random(nsamples)
    price = pd.Series(index=tt, data=vv)
    # logger.debug(F"price = {price}")
    mq = multiquantize(price, t0, nscales=(scale + 1), nsamples=nsamples, max_resolution=max_resolution)
    assert (mq[0].shape == (scale + 1, nsamples))
    assert all(mq[0][scale] == vv)

    logger.info(F"Passed: test_multiquantize")


if __name__ == '__main__':
    test_multiquantize()
    test_windowed_forward_max()
