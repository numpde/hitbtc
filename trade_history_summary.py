# RA, 2020-05-20

import sqlite3
import pandas as pd
from pathlib import Path

# This script requires the sqlite math functions
# form the extension extension-functions.c
# https://www.sqlite.org/contrib
# See folder ./sqlite/extensions/makefile


PARAM = {
    'db': Path(__file__).parent / "db/trade_history/UV/symbols.db",

    'sqlite_ext': (Path(__file__).parent / "sqlite/extensions/UV/").glob("*.so"),

    'db_summary': Path(__file__).parent / "db/trade_history/summary/",
}


def list_sqlite_tables(con) -> pd.DataFrame:
    tables = pd.read_sql_query(con=con, sql="select * from sqlite_master where type='table'")
    assert len(tables)
    return tables


def summary1(con, symbol):
    df = pd.read_sql(con=con, sql=F"""
        SELECT 
            side, 
            avg(quantity), exp(avg(log(quantity))) as 'gavg(quantity)', 
            min(price), max(price), 
            min(timestamp), max(timestamp), 
            count(*)
        FROM (
            SELECT * FROM [{symbol}]
        )
        GROUP BY [side]
    """)
    return df


def drop_duplicates(symbol):
    # HitBTC `id` for duplicate identification
    # SQLite's unique ROWID for selection of a representative from group
    with sqlite3.connect(PARAM['db']) as con:
        sql = F"""
            DELETE FROM {symbol}
            WHERE ROWID NOT IN (
                SELECT MIN(ROWID) FROM {symbol} GROUP BY `id`
            )
        """
        con.execute(sql)


def main():
    with sqlite3.connect(PARAM['db']) as con:
        for ext in PARAM['sqlite_ext']:
            con.load_extension(str(ext))

        for symbol in list_sqlite_tables(con).name:
            # drop_duplicates(symbol)
            df = summary1(con, symbol)
            file = PARAM['db_summary'] / F"{symbol}.EXT"
            file.parent.mkdir(parents=True, exist_ok=True)
            df.to_markdown(file.with_suffix(".txt").open('w'))
            df.to_csv(file.with_suffix(".csv"))


if __name__ == '__main__':
    main()
