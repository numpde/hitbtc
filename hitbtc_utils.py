# RA, 2020-06-04

from commons import logger
from commons import assert_str
from commons import singleton
# from commons import API_URLS
# from commons import Side

from hitbtc_response_specs import API_URLS, ResponseSpecs, Side

import os
import pandas as pd
import requests

from enum import Enum
from dotenv import load_dotenv as _load_dotenv
from typing import Dict
from decimal import Decimal
from datetime import datetime
from functools import lru_cache as in_memory_cache
from contextlib import AbstractContextManager
from urllib.parse import urljoin as _urljoin


class OrderExpiredError(RuntimeError):
    pass


class OrderType(Enum):
    Limit = "limit"
    Market = "market"
    StopLimit = "stopLimit"
    StopMarket = "stopMarket"


class TimeInForce(Enum):
    ImmediateOrCancel = "IOC"
    FillOrKill = "FOK"
    GoodTillCancelled = "GTC"
    Day = "Day"
    GoodTillDate = "GTD"


class Session(requests.Session, AbstractContextManager):
    """
    Context manager for requests.session()
    with the option to use `auth = keys`
    """

    API_URL_PREFIX = "https://api.hitbtc.com/api/2/"

    API_KEY = _load_dotenv() and os.getenv("API_KEY")
    API_KEY_FIELD = "api_key"

    _API_SECRET = _load_dotenv() and os.getenv("SECRET_KEY")

    def __init__(self, private=False):
        super(Session, self).__init__()

        self.private = private

        if self.private:
            self.auth = (self.API_KEY, self._API_SECRET)

        self.raise_for_status = True
        self.last_request_url = None
        self.last_request_json = None
        self.last_response = None

    # def __enter__(self):
    #     return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    # Overload method to prefix url with API_PREFIX
    def request(self, method, url, *args, **kwargs):
        url = url.lstrip("/")

        self.last_request_url = url

        self.last_response = super(Session, self).request(
            method=method, url=_urljoin(self.API_URL_PREFIX + "/", url),
            *args, **kwargs
        )

        # # Session talks anyway
        # logger.debug(F"Response status code: {self.last_response.status_code}")

        try:
            self.last_request_json = self.last_response.json()
        except:
            self.last_request_json = None

        if self.raise_for_status:
            self.last_response.raise_for_status()

        return self.last_response

    @property
    def df(self):
        assert self.last_request_url is not None
        assert self.last_request_json is not None

        # logger.debug(F"Packaging API response: {self.last_request_json}")
        df = ResponseSpecs.match(self.last_request_url).parse(self.last_request_json)

        if self.private:
            # TODO: check type of df
            df[self.API_KEY_FIELD] = self.API_KEY

        return df


def test_response_parser():
    logger.debug(F"Testing response parser")
    with Session() as session:
        session.get(API_URLS['12 recent trades'].format(symbols="BTCUSD,ETHBTC"))
        logger.info(F"Got {session.df}")


@in_memory_cache
def get_symbol_info(symbol=None) -> pd.DataFrame:
    with Session(private=False) as session:
        session.get(API_URLS['symbol info'])
        df = session.df.set_index('id')
        return (df.T[symbol] if symbol else df)


@in_memory_cache
def get_user_commission(symbol) -> pd.Series:
    with Session(private=True) as session:
        session.get(API_URLS['user commission'].format(symbol=symbol))
        return session.df


def hitbtc_strftime(dt: datetime):
    """
    Format datetime for HitBTC
    """
    return dt.strftime("{}T{}Z".format("%Y-%m-%d", "%H:%M:%S.%f"))


def get_tickers(symbol: str, session: Session) -> pd.Series:
    assert symbol
    assert singleton(symbol.split(',')), "Only one symbol expected"
    assert session is not None
    session.get(API_URLS['tickers for symbol'].format(symbol=symbol))
    return session.df


def create_order_json(
        symbol: str,
        side: Side,
        order_type: OrderType,
        time_in_force: TimeInForce,
        quantity: Decimal,
        price: Decimal,
        stop_price: Decimal = None,
        expire_time: datetime = None,
        post_only: bool = True,
        strict_validate: bool = True,
        client_order_id: str = None,
):
    order = {
        # 'clientOrderId': None,
        'symbol': assert_str(symbol),
        'side': assert_str(side.value),
        'type': assert_str(order_type.value),
        'timeInForce': assert_str(time_in_force.value),
        'quantity': str(quantity),
        'strictValidate': strict_validate,
        'postOnly': post_only,
    }

    # price / Number / Order price. Required for limit order types
    if order_type in [OrderType.Limit, OrderType.StopLimit]:
        assert price

    if price:
        order['price'] = str(price)

    # stopPrice / Number / Required for stop-limit orders
    if order_type in [OrderType.StopLimit]:
        assert stop_price

    if stop_price:
        order['stopPrice'] = str(stop_price)

    # expireTime / Datetime / Required for orders with timeInForce = GTD
    if (time_in_force == TimeInForce.GoodTillDate):
        assert expire_time

    if expire_time:
        order['expireTime'] = hitbtc_strftime(expire_time)

    if client_order_id:
        order['clientOrderId'] = client_order_id

    logger.debug(F"Created order JSON: {order}")

    return order


def place_order(order: dict, session: Session) -> pd.Series:
    """
    Note:
    For orders with timeInForce = IOC or FOK, the REST API returns final order state: filled or expired.
    If order can be instantly executed, then the REST API returns a status of filled or partiallyFilled in the order's info.
    """
    logger.debug(F"Placing order: {order}")
    response_code = session.post(API_URLS['place order'], data=order).status_code
    answer = session.df
    logger.debug(F"Response code {response_code} with payload {answer.to_dict()}")
    logger.debug(F"Returned with order status '{answer['status']}'")
    return answer


def get_user_balance(session: Session) -> pd.DataFrame:
    """
    Returns a table df with the balance. Usage:
        df['USD'].available
        df['USD'].reserved
    """

    session.get(API_URLS['user balance'])
    df = session.df

    return df.set_index('currency').T


def get_recent_trades(symbol: str, session: Session) -> pd.DataFrame:
    """
    A table with a handful of recent trades.
    Most recent trades first.
    """
    assert symbol
    assert singleton(symbol.split(',')), "Only one symbol expected"

    session.get(API_URLS['12 recent trades'].format(symbols=symbol))
    df = session.df[symbol]
    assert len(df)

    df = df.set_index('id')
    df = df.sort_values(by='timestamp', ascending=False)
    return df


def _get_order_book(symbols: list, session: Session, limit=5) -> dict:
    url = API_URLS['order book'].format(symbols=",".join(symbols), limit=limit)
    session.get(url)
    return session.df


def get_order_book(symbol: str, session: Session) -> Dict[str, pd.Series]:
    assert session
    book = _get_order_book([symbol], session, limit=50)

    ask: pd.DataFrame
    bin: pd.DataFrame
    ask = book[symbol].ask.set_index('price')['size'].sort_index(ascending=True)
    bid = book[symbol].bid.set_index('price')['size'].sort_index(ascending=False)

    logger.debug(F"Order book ask: \n{ask.cumsum().to_markdown()}")
    logger.debug(F"Order book bid: \n{bid.cumsum().to_markdown()}")

    return {'ask': ask, 'bid': bid}
