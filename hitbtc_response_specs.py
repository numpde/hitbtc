# RA, 2020-05-27

"""
Data type model for HitBTC API responses
"""

"""
TODO:

Handle order book
    https://api.hitbtc.com/#order-book

Handle response from
    public/trades
vs
    public/trades/{symbol}
"""

import numpy as np
import pandas as pd

from decimal import Decimal
from enum import Enum
from functools import lru_cache as memoize
from urllib.parse import urlparse as _urlparse

API_URLS = {
    '12 recent trades': "public/trades?symbols={symbols}&limit=12",
    'symbol info': "public/symbol",
    'tickers for symbol': "public/ticker/{symbol}",
    'order book': "public/orderbook?symbols={symbols}&limit={limit}",

    'place order': "order",
    'order status by id': "order/{clientOrderId}",
    'delete order by id': "order/{clientOrderId}",
    'user balance': "trading/balance",
    'user commission': "trading/fee/{symbol}",
    'user trades': "history/trades?symbol={symbol}&limit=1000",
}


def singleton(collection):
    """
    If the collection has only one distinct element
    return it, otherwise raise an AssertionError
    """
    collection = set(collection)
    assert (1 == len(collection)), F"Series is not a singleton set (len = {len(collection)})"
    return collection.pop()


@memoize
def get_class_children(klass):
    """
    For a class `klass`, enumerates the attributes that
    are not inherited, and do not begin with an underscore

    Returns a dictionary attr_name -> attr
    """
    children = {
        k: getattr(klass, k)
        for k in dir(klass)
        if (not k.startswith('_')) and not (
                k in set.union(*[set(get_class_children(b).keys()) for b in klass.__bases__])
        )
    }
    # logger.debug(F"get_class_children({klass})")
    return children


class Side(Enum):
    Sell = "sell"
    Buy = "buy"

    @staticmethod
    def other(x):
        return {Side.Sell: Side.Buy, Side.Buy: Side.Sell}[x]

    @staticmethod
    def parse(x: str):
        return {Side.Sell.value: Side.Sell, Side.Buy.value: Side.Buy}[x.lower()]

    @staticmethod
    def unparse(x) -> str:
        return x.value


def parse_or_call(t):
    if hasattr(t, 'parse'):
        return t.parse
    else:
        return t.__call__


class ResponseTypes:
    class DataFrame:  # (metaclass=CallParse):
        @classmethod
        def parse(cls, j):
            return cls.apply(pd.DataFrame(data=j))

        @classmethod
        def apply(cls, df: pd.DataFrame):
            for (c, t) in get_class_children(cls).items():
                if c in df.columns:
                    df[c] = df[c].apply(parse_or_call(t))
            return df

    class Series:
        @classmethod
        def parse(cls, j):
            return cls.apply(pd.Series(data=j))

        @classmethod
        def apply(cls, se: pd.Series):
            for (c, t) in get_class_children(cls).items():
                if c in se.index:
                    se[c] = parse_or_call(t)(se[c])
            return se

    class Dict:
        @classmethod
        def parse(cls, j: dict):
            sub = singleton(get_class_children(cls).values())
            return {k: sub.parse(v) for (k, v) in j.items()}

    # @classmethod
    # def Generic(cls, T):
    #     class P:
    #         @classmethod
    #         def parse(cls, t):
    #             return T(t)
    #
    #     return P


class ResponseSpecs:
    @classmethod
    def match(cls, id: str):
        response_spec = singleton({
            response_spec
            for response_spec in get_class_children(ResponseSpecs).values()
            if
            (type(response_spec) is type) and
            _urlparse(id).path.startswith(_urlparse(response_spec._id).path)
        })
        return response_spec

    class PublicSymbol(ResponseTypes.DataFrame):
        # https://api.hitbtc.com/#symbols
        _id = API_URLS['symbol info']
        id = str
        baseCurrency = str
        quoteCurrency = str
        quantityIncrement = Decimal
        tickSize = Decimal
        takeLiquidityRate = Decimal
        provideLiquidityRate = Decimal
        feeCurrency = str

    class TradesBySymbol(ResponseTypes.Dict):
        # https://api.hitbtc.com/#trades
        _id = API_URLS['12 recent trades']

        class Data(ResponseTypes.DataFrame):
            id = int
            price = Decimal
            quantity = Decimal
            side = Side.parse
            timestamp = pd.to_datetime

    class OrderBookBySymbol(ResponseTypes.Dict):
        # https://api.hitbtc.com/#order-book
        _id = API_URLS['order book']

        class Data(ResponseTypes.Series):
            symbol = str
            timestamp = pd.to_datetime

            class ask(ResponseTypes.DataFrame):
                price = Decimal
                size = Decimal

            class bid(ResponseTypes.DataFrame):
                price = Decimal
                size = Decimal

    class Tickers(ResponseTypes.Series):
        # https://api.hitbtc.com/#tickers
        _id = API_URLS['tickers for symbol'].format(symbol="")

        ask = Decimal  # Best ask price. Can return 'null' if no data
        bid = Decimal  # Best bid price. Can return 'null' if no data
        last = Decimal  # Last trade price. Can return 'null' if no data
        open = Decimal  # Last trade price 24 hours ago. Can return 'null' if no data
        low = Decimal  # Lowest trade price within 24 hours
        high = Decimal  # Highest trade price within 24 hours
        volume = Decimal  # Total trading amount within 24 hours in base currency
        volumeQuote = Decimal  # Total trading amount within 24 hours in quote currency
        timestamp = pd.to_datetime  # Last update or refresh ticker timestamp
        symbol = str  # Symbol name

    class TradingBalance(ResponseTypes.DataFrame):
        # https://api.hitbtc.com/#trading-balance
        _id = API_URLS['user balance']
        currency = str
        available = Decimal
        reserved = Decimal

    class Commission(ResponseTypes.Series):
        # https://api.hitbtc.com/#get-trading-commission
        _id = API_URLS['user commission'].format(symbol="")
        takeLiquidityRate = Decimal
        provideLiquidityRate = Decimal

    class NewOrder(ResponseTypes.Series):
        # https://api.hitbtc.com/#create-new-order
        _id = API_URLS['place order']
        clientOrderId = str
        symbol = str
        side = Side.parse
        type = str
        timeInForce = str
        quantity = Decimal
        price = Decimal
        stopPrice = Decimal
        expireTime = pd.to_datetime
        strictValidate = bool
        postOnly = bool

    class UserTrades(ResponseTypes.DataFrame):
        # https://api.hitbtc.com/#trades-history
        _id = API_URLS['user trades']
        id = int
        orderId = int
        clientOrderId = str
        symbol = str
        side = Side.parse
        quantity = Decimal
        price = Decimal
        fee = Decimal
        timestamp = pd.to_datetime
